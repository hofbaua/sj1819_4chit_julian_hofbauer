﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace log4net
{
    class Program
    {

        static Thread[] tar = new Thread[5];
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static void Main(string[] args)
        {
            

            

            tar[0] = new Thread(threadLog);
            tar[1] = new Thread(threadLog);
            tar[2] = new Thread(threadLog);
            tar[3] = new Thread(threadLog);
            tar[4] = new Thread(threadLog);

            tar[0].Name = "T1";
            tar[1].Name = "T2";
            tar[2].Name = "T3";
            tar[3].Name = "T4";
            tar[4].Name = "T5";

            tar[0].Start();
            tar[1].Start();
            tar[2].Start();
            tar[3].Start();
            tar[4].Start();
        }


        static void threadLog()
        {
            Random rand = new Random();
            int x;
            foreach (Thread t in tar)
            {
                x = rand.Next(1, 5);
                switch (x)
                {
                    case 1:
                        log.Info(t.Name + "hat geloggt");
                        break;

                    case 2:
                        log.Warn(t.Name + "hat geloggt");
                        break;

                    case 3:
                        log.Debug(t.Name + "hat geloggt");
                        break;

                    case 4:
                        log.Error(t.Name + "hat geloggt");
                        break;

                    case 5:
                        log.Fatal(t.Name + "hat geloggt");
                        break;
                }
                
            }
                    
        }

       
	

	
    }
}
