﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace WpfApp1
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int xS = 0;
        int yS = 0;
        int count = 0;
        Random rand = new Random();
        public MainWindow()
        {
            InitializeComponent();
            xS = rand.Next(0, 800);
            yS = rand.Next(0, 500);
        }

        public void DrawLine()
        {

            Line myLine = new Line();

            switch (rand.Next(1,4))
            {
                case 1:
                    myLine.Stroke = Brushes.Green;
                    break;
                case 2:
                    myLine.Stroke = Brushes.Red;
                    break;
                case 3:
                    myLine.Stroke = Brushes.Blue;
                    break;
            }

            
            myLine.X1 = xS;
            myLine.X2 = rand.Next(0, 800);
            myLine.Y1 = yS;
            myLine.Y2 = rand.Next(0, 450);
            myLine.StrokeThickness = 2;
            myGrid.Children.Add(myLine);
            xS = (int)myLine.X2;
            yS = (int)myLine.Y2;

            if (count >= 5)
            {
                myGrid.Children.RemoveAt(0);
            }

            count++;
            
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DispatcherTimer dt = new DispatcherTimer();
            dt.Interval = TimeSpan.FromSeconds(2);
            dt.Tick += Dt_Tick;
            dt.Start();

        }

        private void Dt_Tick(object sender, EventArgs e)
        {
            DrawLine();
        }
    }
}
