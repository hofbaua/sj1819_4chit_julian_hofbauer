﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            //NumQuery();
            //ObjectQuery();
            XMLQuery();
        }

        static void NumQuery()
        {
            var numbers = new int[] { 1, 4, 9, 16, 25, 36 };

            var evenNumbers = from p in numbers
                              where (p % 2) == 0
                              select p;

            Console.WriteLine("Result:");
            foreach (var val in evenNumbers)
                Console.WriteLine(val);


        }

        static IEnumerable<Customer> CreateCustomers()
        {
            return new List<Customer>
                                     {
        new Customer { CustomerID = "ALFKI", City = "Berlin"    },
        new Customer { CustomerID = "BONAP", City = "Marseille" },
        new Customer { CustomerID = "CONSH", City = "London"    },
        new Customer { CustomerID = "EASTC", City = "London"    },
        new Customer { CustomerID = "FRANS", City = "Torino"    },
        new Customer { CustomerID = "LONEP", City = "Portland"  },
        new Customer { CustomerID = "NORTS", City = "London"    },
        new Customer { CustomerID = "THEBI", City = "Portland"  }
                                     };
        }

        static void ObjectQuery()
        {
            var results = from c in CreateCustomersXml()
                          where c.City == "London"
                          select c;

            foreach (var c in results)
                Console.WriteLine(c);
        }
        

        static IEnumerable<Customer> CreateCustomersXml()
        {
            return
                from c in XDocument.Load("Customers.xml")
                    .Descendants("Customers").Descendants()
                select new Customer
                {
                    City = c.Attribute("City").Value,
                    CustomerID = c.Attribute("CustomerID").Value
                };
        }

        public static void XMLQuery()
        {
            var doc = XDocument.Load("Customers.xml");

            var results = from c in doc.Descendants("Customer")
                          where c.Attribute("City").Value == "London"
                          select c;

            //Console.WriteLine("Results:\n");
            //foreach (var contact in results)
            //    Console.WriteLine("{0}\n", contact);

            XElement transformedResults =
       new XElement("Londoners",
          from customer in results
          select new XElement("Contact",
             new XAttribute("ID", customer.Attribute("CustomerID").Value),
             new XElement("Name", customer.Attribute("ContactName").Value),
             new XElement("City", customer.Attribute("City").Value)));

            Console.WriteLine("Results:\n{0}", transformedResults);

            transformedResults.Save("Output.xml");


        }



    }
}
