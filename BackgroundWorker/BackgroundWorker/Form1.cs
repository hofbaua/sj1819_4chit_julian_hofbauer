﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace BackgroundWorker
{
    public partial class Form1 : Form
    {

        private string file = @"D:\Schule\4\SEW\Test.txt";

        public Form1()
        {
            InitializeComponent();

            FileStream fs = File.Open(file, FileMode.Create);
            byte b = 97;
            for (int i = 0; i < 20000; i++)
            {
                fs.WriteByte(b++);
                if (b == 123) b = 97;
            }
            fs.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
            textBox1.Refresh();
            
            proFuellen.Maximum = 100;
            proFuellen.Step = 1;
            proFuellen.Value = 0;
            
            this.backgroundWorker1.RunWorkerAsync(file);
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            File.Delete(file);
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

            int var = 0;
            string text = "";
            
            FileStream fs = File.Open(e.Argument.ToString(), FileMode.Open);
            FileInfo fi = new FileInfo(file);
            
            int fileLength = (int)new FileInfo(file).Length;
            int counter = 0;
            while (true)
            {
                var = fs.ReadByte();
                
                if (var == -1)
                {
                    break;
                }
                

                text += Convert.ToChar(var);
                
                counter++;
                backgroundWorker1.WorkerReportsProgress = true;
               
                
                this.backgroundWorker1.ReportProgress(counter * 100 / fileLength);
            }
            e.Result = text;
            fs.Close();
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
           
            proFuellen.Value = e.ProgressPercentage;
        }
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.textBox1.Text = Convert.ToString(e.Result);
            this.proFuellen.Value = 0;
            
        }

    }
}
