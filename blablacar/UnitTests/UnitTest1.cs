﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using blablacar;
using System.Collections.Generic;

namespace UnitTests
{
    [TestClass]
    public class UnitTest1
    {

        [TestMethod]
        public void MitfahrerBucht()
        {
            Mitfahrer m = new Mitfahrer();

            Buchung b = new Buchung() { Mitfahrer = m, Termin = new DateTime(2019, 5, 29), Zahlungsmethode = "Bar", BuchungsId = 1, Fahrt = new Fahrt() { FahrtId = 1, Fahrer = new Fahrer() { FahrerId = 1, Name = "John", Bewertung = 5, Zahlungsmethode = "Bar" }, Strecke = new Strecke() { StreckeId = 1, Von = new Adresse() { AdresseId = 1, lo = 50.33, la = 98.11 }, Bis = new Adresse() { AdresseId = 2, lo = 98.66, la = 71.66 } } } };

            m.Buchen(b);

            Assert.AreEqual(m.buchungen.Count, 1);

        }

        [TestMethod]
        public void MitfahrerGetBuchungen()
        {
            Mitfahrer m = new Mitfahrer();

            Buchung b = new Buchung() { Mitfahrer = m, Termin = new DateTime(2019, 5, 29), Zahlungsmethode = "Bar", BuchungsId = 1, Fahrt = new Fahrt() { FahrtId = 1, Fahrer = new Fahrer() { FahrerId = 1, Name = "John", Bewertung = 5, Zahlungsmethode = "Bar" }, Strecke = new Strecke() { StreckeId = 1, Von = new Adresse() { AdresseId = 1, lo = 50.33, la = 98.11 }, Bis = new Adresse() { AdresseId = 2, lo = 98.66, la = 71.66 } } } };

            m.Buchen(b);

            List<Buchung> t = m.GetBuchung();

            Assert.AreEqual(t[0], b);
        }

        [TestMethod]
        public void MitfahrerCanceled()
        {
            Mitfahrer m = new Mitfahrer();

            Buchung b = new Buchung() { Mitfahrer = m, Termin = new DateTime(2019, 5, 29), Zahlungsmethode = "Bar", BuchungsId = 1, Fahrt = new Fahrt() { FahrtId = 1, Fahrer = new Fahrer() { FahrerId = 1, Name = "John", Bewertung = 5, Zahlungsmethode = "Bar" }, Strecke = new Strecke() { StreckeId = 1, Von = new Adresse() { AdresseId = 1, lo = 50.33, la = 98.11 }, Bis = new Adresse() { AdresseId = 2, lo = 98.66, la = 71.66 } } } };

            m.Buchen(b);

            List<Buchung> t = m.GetBuchung();

            if (t[0] == b)
            {
                m.CancelBuchung(b);

                t = m.GetBuchung();
            }

            Assert.AreEqual(t.Count, 0);
        }

        [TestMethod]
        public void RateFahrt()
        {
            Mitfahrer m = new Mitfahrer();
            Fahrt f = new Fahrt() { FahrtId = 1, Fahrer = new Fahrer() { FahrerId = 1, Name = "John", Bewertung = 5, Zahlungsmethode = "Bar" }, Strecke = new Strecke() { StreckeId = 1, Von = new Adresse() { AdresseId = 1, lo = 50.33, la = 98.11 }, Bis = new Adresse() { AdresseId = 2, lo = 98.66, la = 71.66 } } };
            Buchung b = new Buchung() { Mitfahrer = m, Termin = new DateTime(2019, 5, 29), Zahlungsmethode = "Bar", BuchungsId = 1, Fahrt = f };

            b.RateFahrt(f, 3);

            Assert.AreEqual(f.Fahrer.Bewertung, 3);
        }

        [TestMethod]
        public void RateMitfahrer()
        {
            Mitfahrer m = new Mitfahrer() { Bewertung = 5 };

            Buchung b = new Buchung() { Mitfahrer = m, Termin = new DateTime(2019, 5, 29), Zahlungsmethode = "Bar", BuchungsId = 1, Fahrt = new Fahrt() { FahrtId = 1, Fahrer = new Fahrer() { FahrerId = 1, Name = "John", Bewertung = 5, Zahlungsmethode = "Bar" }, Strecke = new Strecke() { StreckeId = 1, Von = new Adresse() { AdresseId = 1, lo = 50.33, la = 98.11 }, Bis = new Adresse() { AdresseId = 2, lo = 98.66, la = 71.66 } } } };

            b.RateMitfahrer(m, 3);

            Assert.AreEqual(m.Bewertung, 3);
        }

        [TestMethod]
        public void MakeFahrt()
        {
            Fahrt ft = new Fahrt() { FahrtId = 5 };
            Fahrer f = new Fahrer();

            f.MakeFahrt(ft);

            Assert.AreEqual(f.fahrten.Count, 1);
        }

        [TestMethod]
        public void DeleteFahrt()
        {
            Fahrt ft = new Fahrt() { FahrtId = 5 };
            Fahrer f = new Fahrer();

            f.MakeFahrt(ft);

            if (f.fahrten.Count == 1)
            {
                f.DeleteFahrt(ft);
            }

            Assert.AreEqual(f.fahrten.Count, 0);
        }
    }
}
