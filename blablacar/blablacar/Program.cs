﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blablacar
{
    class Program
    {
        static void Main(string[] args)
        {


            using (var ctx = new blablacarContext())
            {
                var fahr = new Fahrer() { FahrerId = 1, Name = "John", Bewertung = 5, Zahlungsmethode = "Bar" };
                var adrvon = new Adresse() { lo = 87.19, la = 68.63 };
                var adrbis = new Adresse() { lo = 63.55, la = 77.69 }; 
                var streck = new Strecke() { StreckeId = 1, Von = adrvon, Bis = adrbis };
                var mitfahr = new Mitfahrer() { MitfahrerId = 1, Name = "James", Strecke = streck};
                var fahrt = new Fahrt() { FahrtId = 1, Fahrer = fahr, Strecke = streck };
                var buch = new Buchung() { BuchungsId = 1, Fahrt = fahrt, Mitfahrer = mitfahr, Termin = new DateTime(2019, 5, 28) };

                ctx.Fahrers.Add(fahr);
                ctx.Adressen.Add(adrvon);
                ctx.Adressen.Add(adrbis);
                ctx.Strecken.Add(streck);
                ctx.Mitfahrers.Add(mitfahr);
                ctx.Fahrten.Add(fahrt);
                ctx.Buchungen.Add(buch);
                ctx.SaveChanges();
            }

        }
    }
}
