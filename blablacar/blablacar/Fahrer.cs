﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace blablacar
{
    public class Fahrer
    {

        public List<Fahrt> fahrten = new List<Fahrt>();

        public int FahrerId
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public int Bewertung
        {
            get; set;
        }

        public string Zahlungsmethode
        {
            get; set;
        }

        

        public void MakeFahrt(Fahrt f)
        {
            fahrten.Add(f);
        }

        public void DeleteFahrt(Fahrt f)
        {
            fahrten.Remove(f);
        }

        
    }
}