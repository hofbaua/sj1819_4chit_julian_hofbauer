﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace blablacar
{
    class blablacarContext : DbContext
    {

        public blablacarContext() : base()
        {
            Database.SetInitializer<blablacarContext>(new DropCreateDatabaseAlways<blablacarContext>());
        }


        public DbSet<Buchung> Buchungen { get; set; }
        public DbSet<Adresse> Adressen { get; set; }
        public DbSet<Fahrer> Fahrers { get; set; }
        public DbSet<Fahrt> Fahrten { get; set; }
        public DbSet<Mitfahrer> Mitfahrers { get; set; }
        public DbSet<Strecke> Strecken { get; set; }

    }
}