﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace blablacar
{
    public class Buchung
    {
        [Key]
        public int BuchungsId
        {
            get; set;
        }

        public System.DateTime Termin
        {
            get; set;
        }

        public string Zahlungsmethode
        {
            get; set;
        }

        
        public Fahrt Fahrt
        {
            get; set;
        }

 
        public Mitfahrer Mitfahrer
        {
            get; set;
        }

        public void RateFahrt(Fahrt f, int b)
        {
            f.Fahrer.Bewertung = b;
        }

        public void RateMitfahrer(Mitfahrer m, int b)
        {
            m.Bewertung = b;
        }
    }
}