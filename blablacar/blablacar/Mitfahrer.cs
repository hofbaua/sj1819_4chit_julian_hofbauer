﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace blablacar
{
    public class Mitfahrer
    {
        public List<Buchung> buchungen = new List<Buchung>();

        public int MitfahrerId
        {
            get; set;
        }
        public Strecke Strecke
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public int Bewertung
        {
            get; set;
        }

        public void Buchen(Buchung b)
        {
            buchungen.Add(b);
        }

        public List<Buchung> GetBuchung()
        {

            return buchungen;
            
        }

        public void CancelBuchung(Buchung b)
        {
            buchungen.Remove(b);
        }


    }
}