﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverWasserstand
{
    class Katastrophenschutz : Observer
    {

        Kienstock ks = new Kienstock();

        int oState = 30;
        private Kienstock _subject;

        public Katastrophenschutz(Kienstock subject)
        {

            _subject = subject;

        }

        public override void Update()
        {
            if (_subject.SubjectState >= oState)
            {
                Console.WriteLine("Katastrophenschutz: Ois Hi");
            }
            else
            {
                Console.WriteLine("Katastrophenschutz: Basst Ois");
            }
        }
    }
}
