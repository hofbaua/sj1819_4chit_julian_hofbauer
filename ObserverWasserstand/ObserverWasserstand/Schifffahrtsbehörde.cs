﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverWasserstand
{
    class Schifffahrtsbehörde : Observer
    {

        Kienstock ks = new Kienstock();

        int oState = 10;
        private Kienstock _subject;

        public Schifffahrtsbehörde(Kienstock subject)
        {

            _subject = subject;

        }

        public override void Update()
        {
            if (_subject.SubjectState >= oState)
            {
                Console.WriteLine("Schifffahrtbehörde: Ois Hi");
            }
            else
            {
                Console.WriteLine("Schifffahrtsbehörde: Basst Ois");
            }
        }
    }
}
