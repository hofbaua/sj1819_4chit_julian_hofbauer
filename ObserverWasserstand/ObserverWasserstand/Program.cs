﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverWasserstand
{
    class Program
    {
        static void Main(string[] args)
        {

            Kienstock ks = new Kienstock();

            ks.Attach(new Landeswarnzentrale(ks));
            ks.Attach(new Katastrophenschutz(ks));
            ks.Attach(new Schifffahrtsbehörde(ks));

            ks.SubjectState = 15;
            ks.Notify();
        }
    }
}
