﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverWasserstand
{
    class Landeswarnzentrale : Observer
    {

        Kienstock ks = new Kienstock();

        int oState = 20;
        private Kienstock _subject;

        public Landeswarnzentrale(Kienstock subject)
        {

            _subject = subject;

        }

        public override void Update()
        {
            if (_subject.SubjectState >= oState)
            {
                Console.WriteLine("Landeswarnzentrale: Ois Hi");
            }
            else
            {
                Console.WriteLine("Landeswarnzentrale: Basst Ois");
            }
        }
    }
}
