﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverWasserstandInterface
{
    class Schifffahrtsbehörde : IObserver
    {

        Kienstock ks = new Kienstock();

        int oState = 10;
        private Kienstock _subject;

        public Schifffahrtsbehörde(Kienstock subject)
        {

            _subject = subject;

        }

        public void Update()
        {
            if (_subject.SubjectState >= oState)
            {
                Console.WriteLine("Schifffahrtbehörde: Ois Hi");
            }
            else
            {
                Console.WriteLine("Schifffahrtsbehörde: Basst Ois");
            }
        }
    }
}
