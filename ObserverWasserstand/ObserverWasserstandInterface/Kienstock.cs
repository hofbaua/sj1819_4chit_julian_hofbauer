﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverWasserstandInterface
{
    class Kienstock : ISubject
    {

        private int _subjectState;

        List<IObserver> observers = new List<IObserver>();
        

        public int SubjectState
        {
            get { return _subjectState; }
            set { _subjectState = value; }
        }

        public void Notify()
        {
            foreach (IObserver o in observers)
            {
                o.Update();
            }
        }

        public void Detach(IObserver o)
        {

            observers.Remove(o);

        }

        public void Attach(IObserver o)
        {
            observers.Add(o);
        }



    }
}
