﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverWasserstandInterface
{
    interface ISubject
    {

        

        void Detach(IObserver o);


        void Attach(IObserver o);


        void Notify();
        
    }
}
