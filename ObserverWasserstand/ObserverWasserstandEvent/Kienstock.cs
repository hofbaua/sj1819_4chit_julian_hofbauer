﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverWasserstandEvent
{
    class Kienstock
    {


        Schifffahrtsbehörde sb = new Schifffahrtsbehörde();
        Landeswarnzentrale lwz = new Landeswarnzentrale();
        Katastrophenschutz kts = new Katastrophenschutz();


        

        public delegate void UpdateDelegate(int wst);

        public event UpdateDelegate UpdateEvent;

        public Kienstock(int wts)
        {
            UpdateEvent += sb.Update;
            UpdateEvent += lwz.Update;
            UpdateEvent += kts.Update;

            Notify(wts);
        }


        public void Notify(int wts)
        {
            UpdateEvent(wts);
        }


       

    }
}
