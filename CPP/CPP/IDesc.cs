﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPP
{
    interface IDesc
    {
        string desc { get; set; }
        int size { get; set; }
    }
}
