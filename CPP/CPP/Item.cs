﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPP
{
    class Item : IDesc
    {

        public string desc { get; set; }
        public int size { get; set; }

        public Item(string desc, int size)
        {
            this.desc = desc;
            this.size = size;
        }
    }
}
