﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPP
{
    class Bag<T> where T:IDesc
    {

        List<T> lt = new List<T>();
        

        int x;

        public Bag(int x)
        {
            this.x = x;
        }

        public void PutItem(T item)
        {
            if (lt.Count < x)
            {
                lt.Add(item);
                Console.WriteLine("Item successfully added");
            }
            else
            {
                Console.WriteLine("Nur "+ x + " Items passen in den Bag");
            }
        }

        public T GetItem()
        {
            if (lt.Count == 0)
            {
                return default(T);
            }
            else
            {
                T t;
                t = lt[0];
                lt.RemoveAt(0);
                return t;
            }
            
        }

        public T GetItemWeighted()
        {

            int h1 = 0;
            List<int> l = new List<int>();

            foreach (T i in lt)
            {
                
                byte[] asciiBytes = Encoding.ASCII.GetBytes(i.ToString());
                foreach (byte b in asciiBytes)
                {
                    h1 += b*i.size;
                    

                }
                l.Add(h1);
                h1 = 0;

            }

            l.Sort((a, b) => -1 * a.CompareTo(b));


            int h2 = 0;


            foreach (T i in lt)
            {

                byte[] asciiBytes = Encoding.ASCII.GetBytes(i.ToString());
                foreach (byte b in asciiBytes)
                {
                    h2 += b * i.size;

                }

                if (l[0] == h2)
                {
                    if (lt.Count == 0)
                    {
                        return default(T);
                    }
                    else
                    {
                        T t;
                        t = i;
                        lt.Remove(i);
                        return t;
                    }
                }
                else
                {
                    h2 = 0;
                }
            }


            return default(T);
          
        }

        public string Content()
        {
            string con = "";

            foreach (T i in lt)
            {
                con = con + i.desc +", ";
            }

            return con;

        }

    }
}
