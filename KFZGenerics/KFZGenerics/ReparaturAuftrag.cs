﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KFZGenerics
{
    class ReparaturAuftrag<T, H> where T:Auto where H:IArtikel
    {

        

        T hersteller;
        H artikel;

        public string vName { get; set; }

        public string nName { get; set; }

        public int lRN { get; set; }

        public Auto herstellerProp { get; set; }

        public List<H> bsTeile = new List<H>();

        public ReparaturAuftrag(string vName, string nName, string autoType, Auto instanz)
        {

            Random rng = new Random();

            lRN = rng.Next(1, 100);

            this.vName = vName;
            this.nName = nName;
            
            Auto newAuto = instanz;

            newAuto.autoType = autoType;



        }
        
        public void ArtikelAdd(H a)
        {
            bsTeile.Add(a);
            a.nachBestellen();
        }

        public override string ToString()
        {
            string help ="Reparaturauftrag: " + lRN + " für Kunde: " + vName + " " + nName + "\n";
            double h = 0;
            foreach (H i in bsTeile)
            {
                help += i.ToString()+"\n";
                h += i.GesPreis();
            }

            help += "\nSumme: "+h;
            

            return help;
        }

    }
}
