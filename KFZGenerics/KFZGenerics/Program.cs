﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KFZGenerics
{
    class Program
    {
        static void Main(string[] args)
        {

            Volvo v = new Volvo();

            ReparaturAuftrag<Volvo, MotorTeile> ra = new ReparaturAuftrag<Volvo, MotorTeile>("Julian", "Hofbauer", "V40", v);

            ra.ArtikelAdd(new MotorTeile("Zylinder", 3, 2));
            ra.ArtikelAdd(new MotorTeile("Öl", 4, 3));

            Console.WriteLine(ra.ToString());
        }
    }
}
