﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KFZGenerics
{
    class MotorTeile : IArtikel
    {

        public int menge { get; set; }

        public double preis { get; set; }

        public string bezeichnung { get; set; }

        public double gesamtPreis { get; set; }

        public MotorTeile(string bezeichnung, int menge, double preis)
        {
            this.bezeichnung = bezeichnung;
            this.menge = menge;
            this.preis = preis;

        }

        public double GesPreis()
        {
            double ges;

            ges = menge * preis;

            return ges;
        }

        public void nachBestellen()
        {
            
        }

        public override string ToString()
        {
            return "\n"+menge + " Stk " + bezeichnung + " - " + GesPreis() + " Euro";
        }

    }
}
