﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericMethods
{
    class Schueler:IComparable
    {

        string name;

        public Schueler(string name)
        {
            this.name = name;
        }

        public int CompareTo(object obj)
        {
            Schueler s = (Schueler)obj;

            return name.CompareTo(s.name);
        }
    }
}
