﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DecoratorWriter
{
    abstract class Decorator : Component
    {

        protected Component cmp;

        public void setCmp(Component cmp)
        {
            this.cmp = cmp;
        }


        public override void Method(StreamWriter sw, string text)
        {
                cmp.Method(sw, "test");
        }

    }
}
