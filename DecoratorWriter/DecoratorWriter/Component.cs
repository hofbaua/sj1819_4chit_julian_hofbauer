﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DecoratorWriter
{
    abstract class Component
    {

        public abstract void Method(StreamWriter sw, string text);

    }
}
