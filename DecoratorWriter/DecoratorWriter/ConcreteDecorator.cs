﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DecoratorWriter
{
    class ConcreteDecorator : Decorator
    {

        public override void Method(StreamWriter sw, string text)
        {
            sw.Write(text.ToUpper());
            sw.Flush();
            sw.Close();
        }

    }
}
