﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DecoratorWriter
{
    class Program
    {
        static void Main(string[] args)
        {

            ConcreteDecorator cd = new ConcreteDecorator();

            StreamWriter sw = new StreamWriter("file.txt");

            cd.Method(sw, "hallo");

        }
    }
}
