﻿#pragma checksum "..\..\Bearbeiten.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "30C38773E6612C182EDB27560535E1881AD20F437AD6D1F6CE4848A58C0B4511"
//------------------------------------------------------------------------------
// <auto-generated>
//     Dieser Code wurde von einem Tool generiert.
//     Laufzeitversion:4.0.30319.42000
//
//     Änderungen an dieser Datei können falsches Verhalten verursachen und gehen verloren, wenn
//     der Code erneut generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using ZeitmanagementToolWPF;


namespace ZeitmanagementToolWPF {
    
    
    /// <summary>
    /// Bearbeiten
    /// </summary>
    public partial class Bearbeiten : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 9 "..\..\Bearbeiten.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid mainGrid;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\Bearbeiten.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton radioOffen;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\Bearbeiten.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton radioErledigt;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\Bearbeiten.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox bearName;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\Bearbeiten.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button bB;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\Bearbeiten.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button bD;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\Bearbeiten.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button bC;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\Bearbeiten.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button bA;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\Bearbeiten.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox bearDesc;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\Bearbeiten.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button hinzOKb;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\Bearbeiten.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button hinzAbbrb;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\Bearbeiten.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button hinzLoe;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ZeitmanagementToolWPF;component/bearbeiten.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\Bearbeiten.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.mainGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.radioOffen = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 3:
            this.radioErledigt = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 4:
            this.bearName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.bB = ((System.Windows.Controls.Button)(target));
            
            #line 14 "..\..\Bearbeiten.xaml"
            this.bB.Click += new System.Windows.RoutedEventHandler(this.Button_Click_B);
            
            #line default
            #line hidden
            return;
            case 6:
            this.bD = ((System.Windows.Controls.Button)(target));
            
            #line 15 "..\..\Bearbeiten.xaml"
            this.bD.Click += new System.Windows.RoutedEventHandler(this.Button_Click_D);
            
            #line default
            #line hidden
            return;
            case 7:
            this.bC = ((System.Windows.Controls.Button)(target));
            
            #line 16 "..\..\Bearbeiten.xaml"
            this.bC.Click += new System.Windows.RoutedEventHandler(this.Button_Click_C);
            
            #line default
            #line hidden
            return;
            case 8:
            this.bA = ((System.Windows.Controls.Button)(target));
            
            #line 17 "..\..\Bearbeiten.xaml"
            this.bA.Click += new System.Windows.RoutedEventHandler(this.Button_Click_A);
            
            #line default
            #line hidden
            return;
            case 9:
            this.bearDesc = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.hinzOKb = ((System.Windows.Controls.Button)(target));
            
            #line 21 "..\..\Bearbeiten.xaml"
            this.hinzOKb.Click += new System.Windows.RoutedEventHandler(this.HinzOKb_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.hinzAbbrb = ((System.Windows.Controls.Button)(target));
            
            #line 22 "..\..\Bearbeiten.xaml"
            this.hinzAbbrb.Click += new System.Windows.RoutedEventHandler(this.HinzAbbrb_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.hinzLoe = ((System.Windows.Controls.Button)(target));
            
            #line 23 "..\..\Bearbeiten.xaml"
            this.hinzLoe.Click += new System.Windows.RoutedEventHandler(this.HinzLoe_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

