﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using TimeToolDLL;

namespace ZeitmanagementToolWPF
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        aufgabenShow aus;
        XmlDocument doc = new XmlDocument();

        Quadrant q = new Quadrant();

        
        public MainWindow()
        {
            InitializeComponent();
            
        }

        private void ButtonShow_Click(object sender, RoutedEventArgs e)
        {

            

            aus = new aufgabenShow(q);


            foreach (Aufgabe a in q.AufgabeL)
            {

                if (a.Dringend == 2 && a.Wichtig == 2)
                {
                    aus.cbA.Items.Add(a.Name);
                }

                if (a.Dringend == 1 && a.Wichtig == 2)
                {
                    aus.cbB.Items.Add(a.Name);
                }

                if (a.Dringend == 2 && a.Wichtig == 1)
                {
                    aus.cbC.Items.Add(a.Name);
                }

                if (a.Dringend == 1 && a.Wichtig == 1)
                {
                    aus.cbD.Items.Add(a.Name);
                }




            }
            aus.ShowDialog();
        }

        private void BearB_Click(object sender, RoutedEventArgs e)
        {

            

            if (aus != null)
            {
                aus.cbA.SelectionChanged += CbA_SelectedIndexChanged;
                aus.cbB.SelectionChanged += CbB_SelectedIndexChanged;
                aus.cbC.SelectionChanged += CbC_SelectedIndexChanged;
                aus.cbD.SelectionChanged += CbD_SelectedIndexChanged;

                

                aus.ShowDialog();

            }
            else
            {
                MessageBox.Show("Keine Elemente zum Bearbeiten");
            }
        }

        private void CbA_SelectedIndexChanged(object sender, EventArgs e)
        {



            Bearbeiten bear = new Bearbeiten();

            bear.ShowDialog();
            aus.Visibility = Visibility.Hidden;

            Aufgabe newAufg = bear.getAufgabe();

            if (bear.cancelled == false)
            {
                if (newAufg.delete)
                {
                    string search = (string)aus.cbA.SelectedItem;
                    int index = q.AufgabeL.IndexOf(q.AufgabeL.First(s => s.Name == search));

                    q.AufgabeL.Remove(q.AufgabeL[index]);
                }
                else
                {
                    string search = (string)aus.cbA.SelectedItem;
                    int index = q.AufgabeL.IndexOf(q.AufgabeL.First(s => s.Name == search));

                    q.AufgabeL[index] = newAufg;
                }
            }
            




        }

        private void CbB_SelectedIndexChanged(object sender, EventArgs e)
        {



            Bearbeiten bear = new Bearbeiten();

            bear.ShowDialog();
            aus.Visibility = Visibility.Hidden;

            Aufgabe newAufg = bear.getAufgabe();

            if (newAufg.delete)
            {
                string search = (string)aus.cbB.SelectedItem;
                int index = q.AufgabeL.IndexOf(q.AufgabeL.First(s => s.Name == search));

                q.AufgabeL.Remove(q.AufgabeL[index]);
            }
            else
            {
                string search = (string)aus.cbB.SelectedItem;
                int index = q.AufgabeL.IndexOf(q.AufgabeL.First(s => s.Name == search));

                q.AufgabeL[index] = newAufg;
            }




        }

        private void CbC_SelectedIndexChanged(object sender, EventArgs e)
        {



            Bearbeiten bear = new Bearbeiten();

            bear.ShowDialog();
            aus.Visibility = Visibility.Hidden;

            Aufgabe newAufg = bear.getAufgabe();

            if (newAufg.delete)
            {
                string search = (string)aus.cbC.SelectedItem;
                int index = q.AufgabeL.IndexOf(q.AufgabeL.First(s => s.Name == search));

                q.AufgabeL.Remove(q.AufgabeL[index]);
            }
            else
            {
                string search = (string)aus.cbC.SelectedItem;
                int index = q.AufgabeL.IndexOf(q.AufgabeL.First(s => s.Name == search));

                q.AufgabeL[index] = newAufg;
            }




        }

        private void CbD_SelectedIndexChanged(object sender, EventArgs e)
        {



            Bearbeiten bear = new Bearbeiten();

            bear.ShowDialog();
            aus.Visibility = Visibility.Hidden;

            Aufgabe newAufg = bear.getAufgabe();

            if (newAufg.delete)
            {
                string search = (string)aus.cbD.SelectedItem;
                int index = q.AufgabeL.IndexOf(q.AufgabeL.First(s => s.Name == search));

                q.AufgabeL.Remove(q.AufgabeL[index]);
            }
            else
            {
                string search = (string)aus.cbD.SelectedItem;
                int index = q.AufgabeL.IndexOf(q.AufgabeL.First(s => s.Name == search));

                q.AufgabeL[index] = newAufg;
            }




        }

        private void HinzB_Click(object sender, RoutedEventArgs e)
        {
            


            Hinzufuegen hz = new Hinzufuegen(q);

            hz.ShowDialog();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {

            
            doc.LoadXml("<Aufgaben></Aufgaben>");

            foreach (Aufgabe a in q.AufgabeL)
            {

                XmlNode aufgabe = doc.CreateElement("Aufgabe");
                XmlNode name = doc.CreateElement("Name");
                name.InnerText = a.Name;
                aufgabe.AppendChild(name);
                XmlNode desc = doc.CreateElement("Beschreibung");
                desc.InnerText = a.Desc;
                aufgabe.AppendChild(desc);
                XmlNode w = doc.CreateElement("Wichtig");
                w.InnerText = a.Wichtig.ToString();
                aufgabe.AppendChild(w);
                XmlNode d = doc.CreateElement("Dringend");
                d.InnerText = a.Dringend.ToString();
                aufgabe.AppendChild(d);
                XmlNode status = doc.CreateElement("Status");
                status.InnerText = a.Status.ToString();
                aufgabe.AppendChild(status);


                XmlNode r = doc.SelectSingleNode("Aufgaben");
                r.AppendChild(aufgabe);



            }

            doc.Save("aufgaben.xml");
        }
    }
}
