﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TimeToolDLL;

namespace ZeitmanagementToolWPF
{
    /// <summary>
    /// Interaktionslogik für Bearbeiten.xaml
    /// </summary>
    public partial class Bearbeiten : Window
    {

        public bool cancelled = false;
        Aufgabe a;
        bool del = false;
        bool priorityChosen;
        int wichtig;
        int dringend;
        public Bearbeiten()
        {
            InitializeComponent();
        }

        private void HinzAbbrb_Click(object sender, EventArgs e)
        {
            cancelled = true;
            this.Close();
        }

        private void HinzOKb_Click(object sender, EventArgs e)
        {
            if (priorityChosen)
            {
                Aufgabe a = new Aufgabe();

                a.Name = bearName.Text;
                a.Desc = bearDesc.Text;


                if (radioOffen.IsChecked == true)
                {
                    a.Status = 1;
                }

                if (radioErledigt.IsChecked == true)
                {
                    a.Status = 2;
                }

                a.Wichtig = wichtig;
                a.Dringend = dringend;

                if (del)
                {
                    a.delete = true;
                }

                this.a = a;

                this.Close();
            }
            else
            {
                MessageBox.Show("Priorität auswählen!");
            }

        }

        public Aufgabe getAufgabe()
        {
            return a;
        }

        private void HinzLoe_Click(object sender, EventArgs e)
        {
            del = true;
            priorityChosen = true;
            this.hinzLoe.Content = "Auf OK drücken.";

        }

        private void Button_Click_A(object sender, RoutedEventArgs e)
        {
            mainGrid.Children.Remove(bB);
            mainGrid.Children.Remove(bC);
            mainGrid.Children.Remove(bD);

            wichtig = 2;
            dringend = 2;

            priorityChosen = true;


        }

        private void Button_Click_B(object sender, RoutedEventArgs e)
        {

            mainGrid.Children.Remove(bA);
            mainGrid.Children.Remove(bC);
            mainGrid.Children.Remove(bD);

            wichtig = 2;
            dringend = 1;

            priorityChosen = true;

        }

        private void Button_Click_C(object sender, RoutedEventArgs e)
        {
            mainGrid.Children.Remove(bB);
            mainGrid.Children.Remove(bA);
            mainGrid.Children.Remove(bD);

            wichtig = 1;
            dringend = 2;

            priorityChosen = true;
        }

        private void Button_Click_D(object sender, RoutedEventArgs e)
        {
            mainGrid.Children.Remove(bB);
            mainGrid.Children.Remove(bC);
            mainGrid.Children.Remove(bA);

            wichtig = 1;
            dringend = 1;

            priorityChosen = true;

        }
    }
}
