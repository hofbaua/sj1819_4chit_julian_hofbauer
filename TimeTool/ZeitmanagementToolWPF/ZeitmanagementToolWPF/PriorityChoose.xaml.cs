﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ZeitmanagementToolWPF
{
    /// <summary>
    /// Interaktionslogik für PriorityChoose.xaml
    /// </summary>
    public partial class PriorityChoose : Window
    {
        public PriorityChoose()
        {
            InitializeComponent();
        }

        public int priority;

        private void Button_Click_A(object sender, EventArgs e)
        {
            priority = 1;

            this.Close();
        }

        private void Button_Click_B(object sender, EventArgs e)
        {
            priority = 2;

            this.Close();
        }

        private void Button_Click_C(object sender, EventArgs e)
        {
            priority = 3;

            this.Close();
        }

        private void Button_Click_D(object sender, EventArgs e)
        {
            priority = 4;

            this.Close();
        }
    }
}
