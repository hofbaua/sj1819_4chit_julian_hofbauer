﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TimeToolDLL;

namespace UnitTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void doesAddItemToList()
        {
            Quadrant q = new Quadrant();
            Aufgabe a = new Aufgabe() { Name = "Test1", Desc = "Dies ist ein Test", Wichtig = 2, Dringend = 2, Status = 1 };

            q.AufgabeL.Add(a);

            q.Add(a);

            Assert.AreEqual(q.AufgabeL[0], q.AufgabeL[1]);

        }
        [TestMethod]
        public void doesRemoveItemFromList()
        {
            Quadrant q = new Quadrant();
            Aufgabe a = new Aufgabe() { Name = "Test1", Desc = "Dies ist ein Test", Wichtig = 2, Dringend = 2, Status = 1 };

            q.AufgabeL.Add(a);

            if (q.AufgabeL != null)
            {
                q.Delete(a);
                Assert.AreNotEqual(q.AufgabeL, null);
            }
            else
            {
                Assert.AreNotEqual(q.AufgabeL, null);
            }
            

            
        }
    }
}
