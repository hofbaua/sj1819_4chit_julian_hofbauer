﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeToolDLL
{
    public class Aufgabe
    {
        public string Name { get; set; }



        public string Desc { get; set; }



        public int Wichtig { get; set; }



        public int Dringend { get; set; }



        public int Status { get; set; } //offen = 1, erledigt = 2

        public bool delete = false;
    }
}
