﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TimeToolDLL;

namespace ZeitmanagementTool
{
    public partial class Hinzufuegen : Form
    {
        

        Quadrant q;

        
       
        public Hinzufuegen(Quadrant q)
        {
            InitializeComponent();

            this.q = q;
        }



        private void HinzOKb_Click(object sender, EventArgs e)
        {
            Aufgabe a = new Aufgabe();

            a.Name = hinzName.Text;
            a.Desc = hinzDesc.Text;
            

            if (radioOffen.Checked)
            {
                a.Status = 1;
            }

            if (radioErledigt.Checked)
            {
                a.Status = 2;
            }

            if (q.priority == 1)
            {
                a.Wichtig = 2;
                a.Dringend = 2;
            }

            if (q.priority == 2)
            {
                a.Wichtig = 2;
                a.Dringend = 1;
            }

            if (q.priority == 3)
            {
                a.Wichtig = 1;
                a.Dringend = 2;
            }

            if (q.priority == 4)
            {
                a.Wichtig = 1;
                a.Dringend = 1;
            }


            q.Add(a);


            this.Close();
        }

        private void HinzAbbrb_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        
    }
}
