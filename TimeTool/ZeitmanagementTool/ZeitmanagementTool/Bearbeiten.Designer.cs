﻿namespace ZeitmanagementTool
{
    partial class Bearbeiten
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dringlichkeitUpDown = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.wichtigkeitUpDown = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.bearDesc = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.bearName = new System.Windows.Forms.TextBox();
            this.radioOffen = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioErledigt = new System.Windows.Forms.RadioButton();
            this.hinzAbbrb = new System.Windows.Forms.Button();
            this.hinzOKb = new System.Windows.Forms.Button();
            this.hinzLoe = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dringlichkeitUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wichtigkeitUpDown)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dringlichkeitUpDown
            // 
            this.dringlichkeitUpDown.Location = new System.Drawing.Point(192, 253);
            this.dringlichkeitUpDown.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.dringlichkeitUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.dringlichkeitUpDown.Name = "dringlichkeitUpDown";
            this.dringlichkeitUpDown.Size = new System.Drawing.Size(120, 20);
            this.dringlichkeitUpDown.TabIndex = 20;
            this.dringlichkeitUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(118, 255);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Dringlichkeit";
            // 
            // wichtigkeitUpDown
            // 
            this.wichtigkeitUpDown.Location = new System.Drawing.Point(192, 193);
            this.wichtigkeitUpDown.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.wichtigkeitUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.wichtigkeitUpDown.Name = "wichtigkeitUpDown";
            this.wichtigkeitUpDown.Size = new System.Drawing.Size(120, 20);
            this.wichtigkeitUpDown.TabIndex = 18;
            this.wichtigkeitUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(118, 195);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Wichtigkeit";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(118, 123);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Beschreibung";
            // 
            // bearDesc
            // 
            this.bearDesc.Location = new System.Drawing.Point(192, 120);
            this.bearDesc.Name = "bearDesc";
            this.bearDesc.Size = new System.Drawing.Size(100, 20);
            this.bearDesc.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(118, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Name";
            // 
            // bearName
            // 
            this.bearName.Location = new System.Drawing.Point(192, 59);
            this.bearName.Name = "bearName";
            this.bearName.Size = new System.Drawing.Size(100, 20);
            this.bearName.TabIndex = 13;
            // 
            // radioOffen
            // 
            this.radioOffen.AutoSize = true;
            this.radioOffen.Checked = true;
            this.radioOffen.Location = new System.Drawing.Point(18, 19);
            this.radioOffen.Name = "radioOffen";
            this.radioOffen.Size = new System.Drawing.Size(51, 17);
            this.radioOffen.TabIndex = 9;
            this.radioOffen.TabStop = true;
            this.radioOffen.Text = "Offen";
            this.radioOffen.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioErledigt);
            this.groupBox1.Controls.Add(this.radioOffen);
            this.groupBox1.Location = new System.Drawing.Point(175, 292);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 100);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            // 
            // radioErledigt
            // 
            this.radioErledigt.AutoSize = true;
            this.radioErledigt.Location = new System.Drawing.Point(17, 53);
            this.radioErledigt.Name = "radioErledigt";
            this.radioErledigt.Size = new System.Drawing.Size(60, 17);
            this.radioErledigt.TabIndex = 10;
            this.radioErledigt.Text = "Erledigt";
            this.radioErledigt.UseVisualStyleBackColor = true;
            // 
            // hinzAbbrb
            // 
            this.hinzAbbrb.Location = new System.Drawing.Point(607, 311);
            this.hinzAbbrb.Name = "hinzAbbrb";
            this.hinzAbbrb.Size = new System.Drawing.Size(75, 23);
            this.hinzAbbrb.TabIndex = 23;
            this.hinzAbbrb.Text = "Abbrechen";
            this.hinzAbbrb.UseVisualStyleBackColor = true;
            this.hinzAbbrb.Click += new System.EventHandler(this.HinzAbbrb_Click);
            // 
            // hinzOKb
            // 
            this.hinzOKb.Location = new System.Drawing.Point(479, 311);
            this.hinzOKb.Name = "hinzOKb";
            this.hinzOKb.Size = new System.Drawing.Size(75, 23);
            this.hinzOKb.TabIndex = 22;
            this.hinzOKb.Text = "OK";
            this.hinzOKb.UseVisualStyleBackColor = true;
            this.hinzOKb.Click += new System.EventHandler(this.HinzOKb_Click);
            // 
            // hinzLoe
            // 
            this.hinzLoe.Location = new System.Drawing.Point(542, 369);
            this.hinzLoe.Name = "hinzLoe";
            this.hinzLoe.Size = new System.Drawing.Size(75, 23);
            this.hinzLoe.TabIndex = 24;
            this.hinzLoe.Text = "Löschen";
            this.hinzLoe.UseVisualStyleBackColor = true;
            this.hinzLoe.Click += new System.EventHandler(this.HinzLoe_Click);
            // 
            // Bearbeiten
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.hinzLoe);
            this.Controls.Add(this.dringlichkeitUpDown);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.wichtigkeitUpDown);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.bearDesc);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bearName);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.hinzAbbrb);
            this.Controls.Add(this.hinzOKb);
            this.Name = "Bearbeiten";
            this.Text = "Bearbeiten";
            ((System.ComponentModel.ISupportInitialize)(this.dringlichkeitUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wichtigkeitUpDown)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown dringlichkeitUpDown;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown wichtigkeitUpDown;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox bearDesc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox bearName;
        private System.Windows.Forms.RadioButton radioOffen;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioErledigt;
        private System.Windows.Forms.Button hinzAbbrb;
        private System.Windows.Forms.Button hinzOKb;
        private System.Windows.Forms.Button hinzLoe;
    }
}