﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TimeToolDLL;

namespace ZeitmanagementTool
{
    public partial class Bearbeiten : Form
    {

        Aufgabe a;
        bool del = false;
        public Bearbeiten()
        {
            InitializeComponent();
        }

        private void HinzAbbrb_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void HinzOKb_Click(object sender, EventArgs e)
        {
            Aufgabe a = new Aufgabe();

            a.Name = bearName.Text;
            a.Desc = bearDesc.Text;
            a.Wichtig = (int)wichtigkeitUpDown.Value;
            a.Dringend = (int)dringlichkeitUpDown.Value;

            if (radioOffen.Checked)
            {
                a.Status = 1;
            }

            if (radioErledigt.Checked)
            {
                a.Status = 2;
            }

            if (del)
            {
                a.delete = true;
            }

            this.a = a;

            this.Close();

        }   

        public Aufgabe getAufgabe()
        {
            return a;
        }

        private void HinzLoe_Click(object sender, EventArgs e)
        {
            del = true;
            this.hinzLoe.Text = "Auf OK drücken.";

        }
    }
}
