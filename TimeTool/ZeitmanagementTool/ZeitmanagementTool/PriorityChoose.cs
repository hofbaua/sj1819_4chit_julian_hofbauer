﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZeitmanagementTool
{
    public partial class PriorityChoose : Form
    {
        public PriorityChoose()
        {
            InitializeComponent();
        }

        public int priority;

        private void ButtonA_Click(object sender, EventArgs e)
        {
            priority = 1;

            this.Close();
        }

        private void ButtonB_Click(object sender, EventArgs e)
        {
            priority = 2;

            this.Close();
        }

        private void ButtonC_Click(object sender, EventArgs e)
        {
            priority = 3;

            this.Close();
        }

        private void ButtonD_Click(object sender, EventArgs e)
        {
            priority = 4;

            this.Close();
        }
    }
}
