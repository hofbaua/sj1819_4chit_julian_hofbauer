﻿namespace ZeitmanagementTool
{
    partial class Hinzufuegen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.hinzName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.hinzDesc = new System.Windows.Forms.TextBox();
            this.radioOffen = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioErledigt = new System.Windows.Forms.RadioButton();
            this.hinzOKb = new System.Windows.Forms.Button();
            this.hinzAbbrb = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // hinzName
            // 
            this.hinzName.Location = new System.Drawing.Point(172, 65);
            this.hinzName.Name = "hinzName";
            this.hinzName.Size = new System.Drawing.Size(100, 20);
            this.hinzName.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(98, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(98, 129);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Beschreibung";
            // 
            // hinzDesc
            // 
            this.hinzDesc.Location = new System.Drawing.Point(172, 126);
            this.hinzDesc.Name = "hinzDesc";
            this.hinzDesc.Size = new System.Drawing.Size(100, 20);
            this.hinzDesc.TabIndex = 2;
            // 
            // radioOffen
            // 
            this.radioOffen.AutoSize = true;
            this.radioOffen.Checked = true;
            this.radioOffen.Location = new System.Drawing.Point(18, 19);
            this.radioOffen.Name = "radioOffen";
            this.radioOffen.Size = new System.Drawing.Size(51, 17);
            this.radioOffen.TabIndex = 9;
            this.radioOffen.TabStop = true;
            this.radioOffen.Text = "Offen";
            this.radioOffen.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioErledigt);
            this.groupBox1.Controls.Add(this.radioOffen);
            this.groupBox1.Location = new System.Drawing.Point(155, 298);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 100);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            // 
            // radioErledigt
            // 
            this.radioErledigt.AutoSize = true;
            this.radioErledigt.Location = new System.Drawing.Point(17, 53);
            this.radioErledigt.Name = "radioErledigt";
            this.radioErledigt.Size = new System.Drawing.Size(60, 17);
            this.radioErledigt.TabIndex = 10;
            this.radioErledigt.Text = "Erledigt";
            this.radioErledigt.UseVisualStyleBackColor = true;
            // 
            // hinzOKb
            // 
            this.hinzOKb.Location = new System.Drawing.Point(459, 317);
            this.hinzOKb.Name = "hinzOKb";
            this.hinzOKb.Size = new System.Drawing.Size(75, 23);
            this.hinzOKb.TabIndex = 11;
            this.hinzOKb.Text = "OK";
            this.hinzOKb.UseVisualStyleBackColor = true;
            this.hinzOKb.Click += new System.EventHandler(this.HinzOKb_Click);
            // 
            // hinzAbbrb
            // 
            this.hinzAbbrb.Location = new System.Drawing.Point(587, 317);
            this.hinzAbbrb.Name = "hinzAbbrb";
            this.hinzAbbrb.Size = new System.Drawing.Size(75, 23);
            this.hinzAbbrb.TabIndex = 12;
            this.hinzAbbrb.Text = "Abbrechen";
            this.hinzAbbrb.UseVisualStyleBackColor = true;
            this.hinzAbbrb.Click += new System.EventHandler(this.HinzAbbrb_Click);
            // 
            // Hinzufuegen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.hinzAbbrb);
            this.Controls.Add(this.hinzOKb);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.hinzDesc);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.hinzName);
            this.Name = "Hinzufuegen";
            this.Text = "Hinzufuegen";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox hinzName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox hinzDesc;
        private System.Windows.Forms.RadioButton radioOffen;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioErledigt;
        private System.Windows.Forms.Button hinzOKb;
        private System.Windows.Forms.Button hinzAbbrb;
    }
}