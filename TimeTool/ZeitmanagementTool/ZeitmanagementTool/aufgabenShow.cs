﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TimeToolDLL;

namespace ZeitmanagementTool
{
    public partial class aufgabenShow : Form
    {

        Quadrant q;
        public aufgabenShow(Quadrant q)
        {
            InitializeComponent();
            this.q = q;
            
        }

        private void HideErl_Click(object sender, EventArgs e)
        {

            this.Controls.Remove(hideErl);

            int ha = 0;
            int hb = 0;
            int hc = 0;
            int hd = 0;
            List<int> il = new List<int>();
            IEnumerable<Aufgabe> results = q.AufgabeL.Where(s => s.Status == 2);
            

            foreach (Aufgabe a in results)
            {
                il.Add(q.AufgabeL.IndexOf(q.AufgabeL.First(s => s == a)));
            }

            foreach (Aufgabe a in results)
            {
                if (a.Wichtig == 2 && a.Dringend == 2)
                {
                    cbA.Items.RemoveAt(il[ha]);
                    ha++;
                }

                if (a.Wichtig == 2 && a.Dringend == 1)
                {
                    cbB.Items.RemoveAt(il[hb]);
                    hb++;
                }

                if (a.Wichtig == 1 && a.Dringend == 2)
                {
                    cbC.Items.RemoveAt(il[hc]);
                    hc++;
                }

                if (a.Wichtig == 1 && a.Dringend == 1)
                {
                    cbD.Items.RemoveAt(il[hd]);
                    hd++;
                }
            }

            
            
        }
    }
}
