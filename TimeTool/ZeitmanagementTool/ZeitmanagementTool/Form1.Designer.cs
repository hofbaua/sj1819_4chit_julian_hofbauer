﻿namespace ZeitmanagementTool
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.hinzB = new System.Windows.Forms.Button();
            this.buttonShow = new System.Windows.Forms.Button();
            this.bearB = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // hinzB
            // 
            this.hinzB.Location = new System.Drawing.Point(374, 58);
            this.hinzB.Name = "hinzB";
            this.hinzB.Size = new System.Drawing.Size(75, 23);
            this.hinzB.TabIndex = 9;
            this.hinzB.Text = "Hinzufügen";
            this.hinzB.UseVisualStyleBackColor = true;
            this.hinzB.Click += new System.EventHandler(this.HinzB_Click);
            // 
            // buttonShow
            // 
            this.buttonShow.Location = new System.Drawing.Point(330, 197);
            this.buttonShow.Name = "buttonShow";
            this.buttonShow.Size = new System.Drawing.Size(161, 40);
            this.buttonShow.TabIndex = 20;
            this.buttonShow.Text = "Liste anzeigen";
            this.buttonShow.UseVisualStyleBackColor = true;
            this.buttonShow.Click += new System.EventHandler(this.ButtonShow_Click);
            // 
            // bearB
            // 
            this.bearB.Location = new System.Drawing.Point(374, 111);
            this.bearB.Name = "bearB";
            this.bearB.Size = new System.Drawing.Size(75, 23);
            this.bearB.TabIndex = 21;
            this.bearB.Text = "Bearbeiten";
            this.bearB.UseVisualStyleBackColor = true;
            this.bearB.Click += new System.EventHandler(this.BearB_Click);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(374, 320);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 22;
            this.saveButton.Text = "Speichern";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.bearB);
            this.Controls.Add(this.buttonShow);
            this.Controls.Add(this.hinzB);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button hinzB;
        private System.Windows.Forms.Button buttonShow;
        private System.Windows.Forms.Button bearB;
        private System.Windows.Forms.Button saveButton;
    }
}

