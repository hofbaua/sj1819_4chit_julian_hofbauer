﻿namespace ZeitmanagementTool
{
    partial class aufgabenShow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.cbB = new System.Windows.Forms.ComboBox();
            this.cbA = new System.Windows.Forms.ComboBox();
            this.cbD = new System.Windows.Forms.ComboBox();
            this.cbC = new System.Windows.Forms.ComboBox();
            this.hideErl = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(614, 223);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 55);
            this.label4.TabIndex = 23;
            this.label4.Text = "C";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(128, 223);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 55);
            this.label3.TabIndex = 22;
            this.label3.Text = "D";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(128, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 55);
            this.label2.TabIndex = 21;
            this.label2.Text = "B";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(617, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 55);
            this.label1.TabIndex = 20;
            this.label1.Text = "A";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // cbB
            // 
            this.cbB.FormattingEnabled = true;
            this.cbB.Location = new System.Drawing.Point(91, 91);
            this.cbB.Name = "cbB";
            this.cbB.Size = new System.Drawing.Size(121, 21);
            this.cbB.TabIndex = 24;
            // 
            // cbA
            // 
            this.cbA.FormattingEnabled = true;
            this.cbA.Location = new System.Drawing.Point(577, 91);
            this.cbA.Name = "cbA";
            this.cbA.Size = new System.Drawing.Size(121, 21);
            this.cbA.TabIndex = 25;
            // 
            // cbD
            // 
            this.cbD.FormattingEnabled = true;
            this.cbD.Location = new System.Drawing.Point(91, 316);
            this.cbD.Name = "cbD";
            this.cbD.Size = new System.Drawing.Size(121, 21);
            this.cbD.TabIndex = 26;
            // 
            // cbC
            // 
            this.cbC.FormattingEnabled = true;
            this.cbC.Location = new System.Drawing.Point(577, 316);
            this.cbC.Name = "cbC";
            this.cbC.Size = new System.Drawing.Size(121, 21);
            this.cbC.TabIndex = 27;
            // 
            // hideErl
            // 
            this.hideErl.Location = new System.Drawing.Point(314, 194);
            this.hideErl.Name = "hideErl";
            this.hideErl.Size = new System.Drawing.Size(165, 23);
            this.hideErl.TabIndex = 28;
            this.hideErl.Text = "Erledigte Aufgaben verstecken";
            this.hideErl.UseVisualStyleBackColor = true;
            this.hideErl.Click += new System.EventHandler(this.HideErl_Click);
            // 
            // aufgabenShow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.hideErl);
            this.Controls.Add(this.cbC);
            this.Controls.Add(this.cbD);
            this.Controls.Add(this.cbA);
            this.Controls.Add(this.cbB);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "aufgabenShow";
            this.Text = "aufgabenShow";
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.BindingSource bindingSource1;
        public System.Windows.Forms.ComboBox cbB;
        public System.Windows.Forms.ComboBox cbA;
        public System.Windows.Forms.ComboBox cbD;
        public System.Windows.Forms.ComboBox cbC;
        private System.Windows.Forms.Button hideErl;
    }
}