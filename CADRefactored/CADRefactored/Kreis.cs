﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace CADRefactored
{
    public class Kreis : Form
    {

        CircleWindow cw = CircleWindow.GetInstance;
        public event EventHandler Click;

        public Kreis()
        {
            shape = "Kreis";
        }

        public override Shape DrawShape(Canvas c)
        {
            
            Ellipse myEllipse = new Ellipse();
            myEllipse.Stroke = Brushes.Black;
            myEllipse.Width = cw.r;
            myEllipse.Height = cw.r;
            Canvas.SetTop(myEllipse, cw.y);
            Canvas.SetLeft(myEllipse, cw.x);
            width = cw.r;
            height = cw.r;
            x = cw.x;
            y = cw.y;
            myEllipse.HorizontalAlignment = HorizontalAlignment.Left;
            myEllipse.VerticalAlignment = VerticalAlignment.Center;
            myEllipse.StrokeThickness = 2;
            c.Children.Add(myEllipse);

            return myEllipse;
        }

        public int x { get; set; }
        public int y { get; set; }
        public int height { get; set; }
        public int width { get; set; }
        public string timestamp { get; set; }

        public bool AmISelected(double mx, double my, Kreis k)
        {
            if (k.x <= mx && k.x + k.width * 2 >= mx && k.y <= my && k.y + k.height * 2 >= my)
            {
                return true;
            }
            return false;
        }
    }
}