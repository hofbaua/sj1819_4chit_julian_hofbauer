﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;


namespace CADRefactored
{
    public abstract class Form
    {
        public SolidColorBrush StrokeColor
        {
            get;
            set;
        }


        public string shape
        {
            get;
            set;
        }

        public int StrokeThickness = 3;

        public abstract Shape DrawShape(Canvas c);

    }
}