﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CADRefactored
{
    /// <summary>
    /// Interaktionslogik für RectWindow.xaml
    /// </summary>
    public partial class RectWindow : Window
    {

        private static RectWindow instance = null;

        public static RectWindow GetInstance
        {
            get
            {
                if (instance == null)
                    instance = new RectWindow();
                return instance;


            }
        }

        public int x;
        public int y;
        public int h;
        public int w;

        public RectWindow()
        {
            InitializeComponent();
        }

        private void RectangleWindowOK_Click(object sender, RoutedEventArgs e)
        {
            x = Convert.ToInt32(xR.Text);
            y = Convert.ToInt32(yR.Text);
            h = Convert.ToInt32(hR.Text);
            w = Convert.ToInt32(wR.Text);

            this.Visibility = Visibility.Hidden;
        }
    }
}
