﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace CADRefactored
{
    public class SelectedDecCircle : ShapeDecorator
    {
        public Kreis Kreis
        {
            get => default(Kreis);
            set
            {
            }
        }
        public SelectedDecCircle(Kreis Kreis):base(Kreis)
        {
            this.Kreis = Kreis;
        }
        public void DecDrawShape(Canvas c, Kreis k)
        {
            
        }
    }
}