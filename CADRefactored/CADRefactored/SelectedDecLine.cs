﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;
using System.Windows.Media;

namespace CADRefactored
{
    public class SelectedDecLine : ShapeDecorator
    {
        public Linie Linie
        {
            get => default(Linie);
            set
            {
            }
        }

        public Canvas c;

        public SelectedDecLine(Linie Linie) : base(Linie)
        {
            this.Linie = Linie;
        }

        public void DecDrawShape(object sender, MouseButtonEventArgs e)
        {
            Rectangle[] rar = new Rectangle[2];

            Line l = (Line)sender;
            for (int i = 0; i < rar.Length; i++)
            {
                rar[i] = new Rectangle();
                rar[i].Width = 10;
                rar[i].Height = 10;
                rar[i].StrokeThickness = 1;
                rar[i].Stroke = Brushes.Black;
            }
            rar[0].Margin = new System.Windows.Thickness(l.X1 - 5, l.Y1 - 5, 0, 0);
            rar[1].Margin = new System.Windows.Thickness(l.X2 - 5, l.Y2 - 5, 0, 0);

            for (int i = 0; i < rar.Length; i++)
            {
                c.Children.Add(rar[i]);
            }
        }
    }
}