﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CADRefactored
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        LineWindow lw = LineWindow.GetInstance;
        RectWindow rw = RectWindow.GetInstance;
        CircleWindow cw = CircleWindow.GetInstance;
        Linie l = new Linie();
        public List<Form> fl = new List<Form>();
        public List<Shape> sl = new List<Shape>();
        


        int x = 0;


        public MainWindow()
        {
            InitializeComponent();
            
        }

        private void lineB_Click(object sender, RoutedEventArgs e)
        {
            lw.Show();

            x = 1;
        }

        private void rectB_Click(object sender, RoutedEventArgs e)
        {
            rw.Show();

            x = 2;
        }

        private void circleB_Click(object sender, RoutedEventArgs e)
        {
            cw.Show();

            x = 3;
        }

        private void DrawButton_Click(object sender, RoutedEventArgs e)
        {
            
            Linie l = new Linie();
            Rechteck r = new Rechteck();
            Kreis k = new Kreis();
            SelectedDecRectangle sdr = new SelectedDecRectangle(r);
            SelectedDecLine sdl = new SelectedDecLine(l);

            if (x != 0)
            {
                switch (x)
                {
                    case 1:
                        sdl.c = canvas1;
                        l.timestamp = DateTime.Now.ToString("yyyy, MM, dd, hh, mm, ss");
                        sl.Add(l.DrawShape(canvas1));
                        fl.Add(l);
                        
                        
                        break;
                    case 2:
                        r.timestamp = DateTime.Now.ToString("yyyy, MM, dd, hh, mm, ss");
                        sl.Add(r.DrawShape(canvas1));
                        fl.Add(r);
                        
                        break;
                    case 3:
                        k.timestamp = DateTime.Now.ToString("yyyy, MM, dd, hh, mm, ss");
                        sl.Add(k.DrawShape(canvas1));
                        fl.Add(k);
                        break;


                }
            }
            else
            {
                MessageBox.Show("Keine Koordinaten angegeben.");
            }
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            Point p = e.GetPosition(this);
            double x = p.X - 112.5;
            double y = p.Y;
            int h = 0;

            foreach (Form f in fl)
            {
                if (f.shape == "Rechteck")
                {
                    Rechteck r = (Rechteck)f;
                    if (r.AmISelected(x, y, r))
                    {
                        rw.Show();
                        rw.xR.Text = r.x.ToString();
                        rw.yR.Text = r.y.ToString();
                        rw.wR.Text = r.width.ToString();
                        rw.hR.Text = r.height.ToString();
                        //canvas1.Children.Remove(sl[h]);
                        //sl.RemoveAt(h);
                        //fl.RemoveAt(h);

                    }
                }
                if (f.shape == "Kreis")
                {
                    Kreis k = (Kreis)f;
                    if (k.AmISelected(x, y, k))
                    {
                        cw.Show();
                        cw.xC.Text = k.x.ToString();
                        cw.yC.Text = k.y.ToString();
                        cw.rC.Text = k.width.ToString();
                        //canvas1.Children.Remove(sl[h]);
                        //sl.RemoveAt(h);
                        //fl.RemoveAt(h);


                    }
                }

                if (f.shape == "Linie")
                {
                    Linie l = (Linie)f;
                    if (l.AmISelected(x, y, l))
                    {
                        lw.Show();
                        lw.tbLx1.Text = l.x1.ToString();
                        lw.tbLy1.Text = l.y1.ToString();
                        lw.tbLx2.Text = l.x2.ToString();
                        lw.tbLy2.Text = l.y2.ToString();
                        //canvas1.Children.Remove(sl[h]);
                        //sl.RemoveAt(h);
                        //fl.RemoveAt(h);

                    }

                }
                h++;
            }
        }




    }
}
