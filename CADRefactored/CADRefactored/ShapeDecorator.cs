﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace CADRefactored
{
    public class ShapeDecorator : Form
    {

        public IForm IForm
        {
            get => default(IForm);
            set
            {
            }
        }

        public override Shape DrawShape(Canvas c)
        {
            Rectangle myRectangle = new Rectangle();

            myRectangle.Height = 10;
            myRectangle.Width = 10;

            myRectangle.StrokeThickness = 2;
            myRectangle.Stroke = System.Windows.Media.Brushes.Black;

            return myRectangle;
        }

        public ShapeDecorator(Form Form)
        {
            this.Form = Form;
        }
    }
}