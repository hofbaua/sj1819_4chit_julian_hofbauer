﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace CADRefactored
{
    public class Rechteck : Form
    {

        public Rechteck()
        {
            shape = "Rechteck";
        }

        RectWindow rw = RectWindow.GetInstance;

        public event EventHandler Click;

        public override Shape DrawShape(Canvas c)
        {
            
            Rectangle myRectangle = new Rectangle();
            myRectangle.Stroke = Brushes.Black;
            myRectangle.Width = rw.w;
            myRectangle.Height = rw.h;
            Canvas.SetTop(myRectangle, rw.y);
            Canvas.SetLeft(myRectangle, rw.x);
            width = rw.w;
            height = rw.h;
            x = rw.x;
            y = rw.y;
            myRectangle.HorizontalAlignment = HorizontalAlignment.Left;
            myRectangle.VerticalAlignment = VerticalAlignment.Center;
            myRectangle.StrokeThickness = 2;

            c.Children.Add(myRectangle);

            return myRectangle;
        }

        

        public int x { get; set; }
        public int y { get; set; }
        public int height { get; set; }
        public int width { get; set; }
        public string timestamp { get; set; }

        public bool AmISelected(double mx, double my, Rechteck r)
        {
            if (r.x <= mx && r.x + r.width >= mx && r.y <= my && r.y + r.height >= my)
            {
                return true;
            }
            return false;
        }
    }
}