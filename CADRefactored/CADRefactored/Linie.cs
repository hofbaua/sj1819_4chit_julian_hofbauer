﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows;

namespace CADRefactored
{
    public class Linie : Form
    {


        public event EventHandler Click;
        LineWindow lw = LineWindow.GetInstance;
        static Linie l = new Linie();
        SelectedDecLine sdl = new SelectedDecLine(l);

        public Linie()
        {
            shape = "Linie";
        }


        public override Shape DrawShape(Canvas c)
        {
            
            Line myLine = new Line();
            
            
            
            myLine.Stroke = Brushes.Black;
            x1 = lw.x1;
            x2 = lw.x2;
            y1 = lw.y1;
            y2 = lw.y2;
            myLine.X1 = x1;
            myLine.X2 = x2;
            myLine.Y1 = y1;
            myLine.Y2 = y2;
            myLine.HorizontalAlignment = HorizontalAlignment.Left;
            myLine.VerticalAlignment = VerticalAlignment.Center;
            myLine.StrokeThickness = 2;
            myLine.MouseLeftButtonDown += sdl.DecDrawShape;
            c.Children.Add(myLine);

            return myLine;
        }

        public int x1 { get; set; }
        public int y1 { get; set; }
        public int x2 { get; set; }
        public int y2 { get; set; }
        public string timestamp { get; set; }

        public bool AmISelected(double mx, double my, Linie l)
        {

            if (l.x1 < l.x2 && l.y1 < l.y2)
            {
                if (l.x1 <= mx && l.x2 >= mx && l.y1 <= my && l.y2 >= my)
                {
                    return true;
                }
                return false;
            }
            if (l.x2 < l.x1 && l.y2 < l.y1)
            {
                if (l.x2 <= mx && l.x1 >= mx && l.y2 <= my && l.y1 >= my)
                {
                    return true;
                }
                return false;
            }
            if (l.x2 < l.x1 && l.y1 < l.y2)
            {
                if (l.x2 <= mx && l.x1 >= mx && l.y1 <= my && l.y2 >= my)
                {
                    return true;
                }
                return false;
            }
            if (l.x1 < l.x2 && l.y2 < l.y1)
            {
                if (l.x1 <= mx && l.x2 >= mx && l.y2 <= my && l.y1 >= my)
                {
                    return true;
                }
                return false;
            }
            else
            {
                return false;
            }


        }
    }
    }
