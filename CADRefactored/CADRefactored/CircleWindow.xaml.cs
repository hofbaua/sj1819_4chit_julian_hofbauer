﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CADRefactored
{
    /// <summary>
    /// Interaktionslogik für CircleWindow.xaml
    /// </summary>
    public partial class CircleWindow : Window
    {

        private static CircleWindow instance = null;

        public static CircleWindow GetInstance
        {
            get
            {
                if (instance == null)
                    instance = new CircleWindow();
                return instance;


            }
        }

        public int x;
        public int y;
        public int r;

        public CircleWindow()
        {
            InitializeComponent();
        }

        private void CircleWindowOK_Click(object sender, RoutedEventArgs e)
        {
            x = Convert.ToInt32(xC.Text);
            y = Convert.ToInt32(yC.Text);
            r = Convert.ToInt32(rC.Text);



            this.Visibility = Visibility.Hidden;
        }
    }
}
