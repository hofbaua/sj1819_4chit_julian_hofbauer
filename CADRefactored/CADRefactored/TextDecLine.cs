﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;

namespace CADRefactored
{
    public class TextDecLine : ShapeDecorator
    {
        public Linie Linie
        {
            get => default(Linie);
            set
            {
            }
        }

        public TextDecLine(Linie Linie) : base(Linie)
        {
            this.Linie = Linie;
        }

        public void DrawShape(object sender, MouseButtonEventArgs e)
        {
            
        }
    }
}