﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace CADRefactored
{
    public class TextDecCircle : ShapeDecorator
    {
        public Kreis Kreis
        {
            get => default(Kreis);
            set
            {
            }
        }

        public TextDecCircle(Kreis Kreis) : base(Kreis)
        {
            this.Kreis = Kreis;
        }

        public void DrawShape(Canvas c)
        {
            
        }
    }
}