﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using TextTicker;

namespace WPFTextTicker
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private TextBlock[,] textblocks = new TextBlock[33, 7];
        private TextTickerC tt = new TextTickerC("I LOVE IT ");

        public MainWindow()
        {
            InitializeComponent();
            for (int i = 0; i < textblocks.GetLength(1); i++)
            {
                for (int j = 0; j < textblocks.GetLength(0); j++)
                {
                    textblocks[j, i] = new TextBlock();
                    textblocks[j, i].SetValue(Grid.ColumnProperty, j);
                    textblocks[j, i].SetValue(Grid.RowProperty, i);

                    TextGrid.Children.Add(textblocks[j, i]);
                }
            }

            FillGrid();
        }

        public void FillGrid()
        {
            for (int i = 0; i < textblocks.GetLength(1); i++)
            {
                for (int j = 0; j < textblocks.GetLength(0); j++)
                {
                    if (tt.textarr[j,i])
                    {
                        textblocks[j, i].Background = new SolidColorBrush(Colors.White);
                    }
                    else
                        textblocks[j, i].Background = new SolidColorBrush(Colors.Black);
                }
            }

            
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DispatcherTimer dt = new DispatcherTimer();
            dt.Interval = TimeSpan.FromMilliseconds(100);
            dt.Tick += Dt_Tick;
            dt.Start();
        }

        private void Dt_Tick(object sender, EventArgs e)
        {
            FillGrid();
            tt.ShiftLeft();
        }
    }
}
