﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TextTicker;

namespace TextTickerForms
{
    public partial class Form1 : Form
    {
        private Panel[,] panels = new Panel[33, 7];
        private TextTickerC tt = new TextTickerC("I LOVE IT ");

        public Form1()
        {
            InitializeComponent();
            int top = 150;
            int left = 0;
            for (int i = 0; i < panels.GetLength(1); i++)
            {
                for (int j = 0; j < panels.GetLength(0); j++)
                {
                    panels[j, i] = new Panel();
                    panels[j,i].Height = 5;
                    panels[j, i].Width = 5;
                    panels[j, i].Top = top;
                    panels[j, i].Left = left;
                    panel1.Controls.Add(panels[j,i]);

                    left += 5;
                    
                }
                left = 0;
                top += 5;
            }

            FillPanel();
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            FillPanel();
        }

        public void FillPanel()
        {
            for (int i = 0; i < panels.GetLength(1); i++)
            {
                for (int j = 0; j < panels.GetLength(0); j++)
                {
                    if (tt.textarr[j, i])
                    {
                        panels[j, i].BackColor = Color.White;
                    }
                    else
                    {
                        panels[j, i].BackColor = Color.Black;   
                    }
                }
            }

            tt.ShiftLeft();
        }
    }
}
