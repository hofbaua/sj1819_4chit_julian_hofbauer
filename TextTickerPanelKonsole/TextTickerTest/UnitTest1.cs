﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TextTickerPanelKonsole;

namespace TextTickerTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void ShiftTest()
        {
            TextTicker tt = new TextTicker("I LOVE IT");
            bool[,] test = (bool[,])tt.textarr.Clone();

            tt.ShiftLeft();
            tt.ShiftRight();

            CollectionAssert.AreEqual(test, tt.textarr);
        }
    }
}
