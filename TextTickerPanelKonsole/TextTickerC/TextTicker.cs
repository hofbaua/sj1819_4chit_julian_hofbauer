﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextTicker
{
    public class TextTickerC
    {
        private string text;
        public bool[,] textarr { get; set; }

        public TextTickerC(string text)
        {
            this.text = text.ToUpper();
            if (text.Length * 4 < 33)
            {
                textarr = new bool[33, 7];
            }
            else
                textarr = new bool[text.Length * 4, 7];

            FillAr();
        }

        private void FillAr()
        {
            bool[,] test = new bool[3, 7];
            int x = 0;
            int y = 0;
            for (int i = 0; i < text.Length; i++)
            {
                switch (text[i])
                {
                    case 'H':
                        {
                            test = ArH();
                            for (int j = 0; j < test.GetLength(0); j++)
                            {
                                for (int k = 0; k < test.GetLength(1) - 1; k++)
                                {
                                    textarr[x, y] = test[j, k];
                                    y++;
                                }
                                y = 0;
                                x++;
                            }
                            x++;
                            y = 0;
                        }

                        break;
                    case 'L':
                        {
                            test = ArL();
                            for (int j = 0; j < test.GetLength(0); j++)
                            {
                                for (int k = 0; k < test.GetLength(1) - 1; k++)
                                {
                                    textarr[x, y] = test[j, k];
                                    y++;
                                }
                                y = 0;
                                x++;
                            }
                            x++;
                            y = 0;
                        }
                        break;
                    case 'T':
                        {
                            test = ArT();
                            for (int j = 0; j < test.GetLength(0); j++)
                            {
                                for (int k = 0; k < test.GetLength(1) - 1; k++)
                                {
                                    textarr[x, y] = test[j, k];
                                    y++;
                                }
                                y = 0;
                                x++;
                            }
                            x++;
                            y = 0;
                        }
                        break;
                    case 'I':
                        {
                            test = ArI();
                            for (int j = 0; j < test.GetLength(0); j++)
                            {
                                for (int k = 0; k < test.GetLength(1) - 1; k++)
                                {
                                    textarr[x, y] = test[j, k];
                                    y++;
                                }
                                y = 0;
                                x++;
                            }
                            x++;
                            y = 0;
                        }
                        break;

                    case 'O':
                        {
                            test = ArO();
                            for (int j = 0; j < test.GetLength(0); j++)
                            {
                                for (int k = 0; k < test.GetLength(1) - 1; k++)
                                {
                                    textarr[x, y] = test[j, k];
                                    y++;
                                }
                                y = 0;
                                x++;
                            }
                            x++;
                            y = 0;
                        }
                        break;
                    case 'V':
                        {
                            test = ArV();
                            for (int j = 0; j < test.GetLength(0); j++)
                            {
                                for (int k = 0; k < test.GetLength(1) - 1; k++)
                                {
                                    textarr[x, y] = test[j, k];
                                    y++;
                                }
                                y = 0;
                                x++;
                            }
                            x++;
                            y = 0;
                        }
                        break;
                    case 'E':
                        {
                            test = ArE();
                            for (int j = 0; j < test.GetLength(0); j++)
                            {
                                for (int k = 0; k < test.GetLength(1) - 1; k++)
                                {
                                    textarr[x, y] = test[j, k];
                                    y++;
                                }
                                y = 0;
                                x++;
                            }
                            x++;
                            y = 0;
                        }
                        break;
                    case ' ':
                        {
                            test = new bool[3, 7];
                            for (int j = 0; j < test.GetLength(0); j++)
                            {
                                for (int k = 0; k < test.GetLength(1) - 1; k++)
                                {
                                    textarr[x, y] = test[j, k];
                                    y++;
                                }
                                y = 0;
                                x++;
                            }
                            x++;
                            y = 0;
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        private bool[,] ArH()
        {
            bool[,] ar = new bool[3, 7];
            ar[0, 1] = true;
            ar[0, 2] = true;
            ar[0, 3] = true;
            ar[0, 4] = true;
            ar[0, 5] = true;

            ar[2, 1] = true;
            ar[2, 2] = true;
            ar[2, 3] = true;
            ar[2, 4] = true;
            ar[2, 5] = true;

            ar[1, 3] = true;

            return ar;
        }

        private bool[,] ArT()
        {
            bool[,] ar = new bool[3, 7];
            ar[1, 1] = true;
            ar[1, 2] = true;
            ar[1, 3] = true;
            ar[1, 4] = true;
            ar[1, 5] = true;

            ar[0, 1] = true;
            ar[2, 1] = true;

            return ar;
        }

        private bool[,] ArL()
        {
            bool[,] ar = new bool[3, 7];
            ar[0, 1] = true;
            ar[0, 2] = true;
            ar[0, 3] = true;
            ar[0, 4] = true;
            ar[0, 5] = true;

            ar[1, 5] = true;

            return ar;
        }

        private bool[,] ArI()
        {
            bool[,] ar = new bool[3, 7];
            ar[1, 1] = true;
            ar[1, 2] = true;
            ar[1, 3] = true;
            ar[1, 4] = true;
            ar[1, 5] = true;

            return ar;
        }

        private bool[,] ArO()
        {
            bool[,] ar = new bool[3, 7];
            ar[0, 1] = true;
            ar[0, 2] = true;
            ar[0, 3] = true;
            ar[0, 4] = true;
            ar[0, 5] = true;

            ar[1, 0] = true;
            ar[1, 5] = true;

            ar[2, 1] = true;
            ar[2, 2] = true;
            ar[2, 3] = true;
            ar[2, 4] = true;
            ar[2, 5] = true;

            return ar;
        }

        private bool[,] ArV()
        {
            bool[,] ar = new bool[3, 7];
            ar[0, 1] = true;
            ar[0, 2] = true;
            ar[0, 3] = true;
            ar[0, 4] = true;
            ar[1, 5] = true;

            ar[2, 1] = true;
            ar[2, 2] = true;
            ar[2, 3] = true;
            ar[2, 4] = true;

            return ar;
        }

        private bool[,] ArE()
        {
            bool[,] ar = new bool[3, 7];
            ar[0, 1] = true;
            ar[0, 2] = true;
            ar[0, 3] = true;
            ar[0, 4] = true;
            ar[0, 5] = true;

            ar[1, 1] = true;
            ar[1, 3] = true;
            ar[1, 5] = true;

            return ar;
        }

        public void ShiftLeft()
        {
            bool[] help = new bool[7];
            for (int i = 0; i < textarr.GetLength(0); i++)
            {
                for (int j = 0; j < textarr.GetLength(1); j++)
                {
                    if (i == 0)
                    {
                        textarr[textarr.GetLength(0) - 1, j] = textarr[i, j];
                    }
                    else
                    {
                        textarr[i - 1, j] = textarr[i, j];
                    }
                }
            }
        }

        public void ShiftRight()
        {
            bool[] help = new bool[7];
            for (int i = textarr.GetLength(0) - 1; i >= 0; i--)
            {
                for (int j = 0; j < textarr.GetLength(1); j++)
                {
                    if (i == textarr.GetLength(0) - 1)
                    {
                        help[j] = textarr[0, j];
                        textarr[0, j] = textarr[i, j];
                    }
                    else
                    if (i == 0)
                    {
                        textarr[i + 1, j] = help[j];
                    }
                    else
                    {
                        textarr[i + 1, j] = textarr[i, j];
                    }
                }
            }
        }
    }
}
