﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Util;


namespace Number2TextTests
{
    [TestClass]
    public class UnitTest1
    {
        

        [TestMethod]
        public void EinhundertEquals100()
        {
            Utili u = new Utili();

            Assert.AreEqual(u.Conv2Text(100), "einhundert");
        }

        [TestMethod]
        public void EintausendEquals1000()
        {
            Utili u = new Utili();
            Assert.AreEqual(u.Conv2Text(1000), "eintausend");
        }

        [TestMethod]
        public void TwelveEquals12()
        {

            Utili u = new Utili();
            Assert.AreEqual(u.Conv2Text(12), "zwölf");
        }

        [TestMethod]
        public void EintausendeinhundertelftausendIs1111()
        {

            Utili u = new Utili();
            Assert.AreEqual(u.Conv2Text(1111), "einstausendeinshundertelf");
        }

        [TestMethod]
        public void ElftauseneinhundertelfIs11111()
        {

            Utili u = new Utili();
            Assert.AreEqual(u.Conv2Text(11111), "elftausendeinshundertelf");
        }



        [TestMethod]
        public void NumberOver500000ThrowsNumberNotConvertableException()
        {
            Utili u = new Utili();

            try
            {
                u.Conv2Text(500001);
            }
            catch (NumberNotConvertableException e)
            {

                Assert.AreEqual(u.HigherThan500000Message, e.Message);
                return;
            }

            Assert.Fail("Exception ned do");
            

            
            
            
        }

        [TestMethod]
        public void NumberUnder0ThrowsNumberNotConvertableException()
        {

            Utili u = new Utili();

            try
            {
                u.Conv2Text(-5);
            }
            catch (NumberNotConvertableException e)
            {

                Assert.AreEqual(u.LowerThan0Message, e.Message);
                return;
            }

            Assert.Fail("Exception ned do");


        }
    }
}
