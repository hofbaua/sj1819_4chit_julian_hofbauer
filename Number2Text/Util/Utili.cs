﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Util
{
    public class Utili
    {

        public Dictionary<Int64, string> Num2Txt = new Dictionary<Int64, string>();
        public string HigherThan500000Message = "Zahl muss kleiner/gleich als 500000 sein";
        public string LowerThan0Message = "Zahl muss größer/gleich als 0 sein";

        public Utili()
        {
            FillDict();
        }

        public string Conv2Text(Int64 num)
        {



            Int64 einer;
            Int64 zehner;
            Int64 hunderter;
            Int64 tausender;
            Int64 ztausender;
            Int64 htausender;

            List<Int64> numl = new List<Int64>();




            if (num > 500000)
                throw new NumberNotConvertableException(HigherThan500000Message);
            else
            if (num < 0)
                throw new NumberNotConvertableException(LowerThan0Message);
            else
            {
                string snum = "Funktioniert nix";

                if (num >= 0 && num < 13)
                {
                    if (Num2Txt.ContainsKey(num % 100))
                    {
                        zehner = num % 100;
                        snum = Num2Txt[zehner];
                    }
                    else
                    {
                        zehner = num % 100;
                        snum = Num2Txt[zehner];
                    }

                }

                if (num >= 13 && num < 100)
                {
                    if (Num2Txt.ContainsKey(num % 100))
                    {
                        zehner = num % 100;
                        snum = Num2Txt[zehner];
                    }
                    else
                    {
                        einer = num % 10;
                        zehner = num / 10;
                        snum = Num2Txt[einer] + "und" + Num2Txt[zehner] + "zig";
                    }
                }


                if (num >= 100 && num < 1000)
                {
                    if (Num2Txt.ContainsKey(num % 1000))
                    {
                        snum = "einhundert";
                        return snum;
                    }
                    if (Num2Txt.ContainsKey(num % 100))
                    {
                        hunderter = num / 100;
                        zehner = num % 100;
                        snum = Num2Txt[hunderter] + "hundert" + Num2Txt[zehner];
                    }
                    else
                    {

                        hunderter = num / 100;
                        zehner = num / 10 - hunderter * 10;
                        einer = num % 100 - zehner * 10;

                        snum = Num2Txt[hunderter] + "hundert" + Num2Txt[einer] + "und" + Num2Txt[zehner] + "zig";
                    }
                }

                if (num >= 1000)
                {
                    if (Num2Txt.ContainsKey(num % 10000))
                    {
                        snum = "eintausend";
                        return snum;
                    }

                    if (Num2Txt.ContainsKey(num % 100))
                    {

                        numl = Split(num);
                        hunderter = numl[0] / 100;
                        zehner = numl[0] % 100;
                        tausender = numl[1] / 1000;
                        snum = Num2Txt[tausender] + "tausend" + Num2Txt[hunderter] + "hundert" + Num2Txt[zehner];
                    }
                    else
                    {
                        numl = Split(num);
                        numl[1] = numl[1] / 1000;
                        htausender = numl[1] / 100;
                        ztausender = numl[1] / 10 % 10;
                        tausender = numl[1] % 10;

                        hunderter = numl[0] / 100;
                        zehner = numl[0] / 10 % 10;
                        einer = numl[0] % 10;


                        if (num < 10000)
                        {
                            snum = Num2Txt[tausender] + "tausend" + Num2Txt[hunderter] + "hundert" + Num2Txt[einer] + "und" + Num2Txt[zehner] + "zig";
                        }

                        if (num >= 10000 && num < 100000)
                        {
                            snum = Num2Txt[tausender] + "und" + Num2Txt[ztausender] + "zigtausend" + Num2Txt[hunderter] + "hundert" + Num2Txt[einer] + "und" + Num2Txt[zehner] + "zig";
                        }
                        if (num >= 100000)
                        {
                            snum = Num2Txt[htausender] + "hundert" + Num2Txt[tausender] + "und" + Num2Txt[ztausender] + "zigtausend" + Num2Txt[hunderter] + "hundert" + Num2Txt[einer] + "und" + Num2Txt[zehner] + "zig";
                        }


                    }

                }





                return snum;
            }
        }

        public void FillDict()
        {

            Num2Txt.Add(1, "eins");
            Num2Txt.Add(2, "zwei");
            Num2Txt.Add(3, "drei");
            Num2Txt.Add(4, "vier");
            Num2Txt.Add(5, "fünf");
            Num2Txt.Add(6, "sechs");
            Num2Txt.Add(7, "sieben");
            Num2Txt.Add(8, "acht");
            Num2Txt.Add(9, "neun");
            Num2Txt.Add(10, "zehn");
            Num2Txt.Add(11, "elf");
            Num2Txt.Add(12, "zwölf");
            Num2Txt.Add(13, "dreizehn");
            Num2Txt.Add(14, "vierzehn");
            Num2Txt.Add(15, "fünfzehn");
            Num2Txt.Add(16, "sechzehn");
            Num2Txt.Add(17, "siebzehn");
            Num2Txt.Add(18, "achtzehn");
            Num2Txt.Add(19, "neunzehn");
            Num2Txt.Add(20, "zwanzig");
            Num2Txt.Add(30, "dreißig");
            Num2Txt.Add(40, "vierzig");
            Num2Txt.Add(50, "fünfzig");
            Num2Txt.Add(60, "sechzig");
            Num2Txt.Add(70, "siebzig");
            Num2Txt.Add(80, "achtzig");
            Num2Txt.Add(90, "neunzig");
            Num2Txt.Add(21, "einundzwanzig");
            Num2Txt.Add(31, "einunddreißig");
            Num2Txt.Add(41, "einundvierzig");
            Num2Txt.Add(51, "einundfünfzig");
            Num2Txt.Add(61, "einundsechzig");
            Num2Txt.Add(71, "einundsiebzig");
            Num2Txt.Add(81, "einundachtzig");
            Num2Txt.Add(91, "einundneunzig");
            Num2Txt.Add(100, "einhundert");
            Num2Txt.Add(200, "zweihundert");
            Num2Txt.Add(300, "dreihundert");
            Num2Txt.Add(400, "vierhundert");
            Num2Txt.Add(500, "fünfhundert");
            Num2Txt.Add(600, "sechshundert");
            Num2Txt.Add(700, "siebenhundert");
            Num2Txt.Add(800, "achthundert");
            Num2Txt.Add(900, "neunhundert");

            Num2Txt.Add(1000, "eintausend");


        }

        public List<Int64> Split(Int64 num)
        {
            List<Int64> numl = new List<Int64>();

            if (num > 1000)
            {
                numl.Add(num % 1000);
                num = num - (num % 1000);
                numl.Add(num);

            }

            return numl;

        }

    }
}
