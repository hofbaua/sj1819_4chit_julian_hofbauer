﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Util
{
    
    public class NumberNotConvertableException : ApplicationException
    {

        
       
            public NumberNotConvertableException() { }
            public NumberNotConvertableException(string message) : base(message) { }
            public NumberNotConvertableException(string message, Exception inner) : base(message, inner) { }

            
        
    }
}
