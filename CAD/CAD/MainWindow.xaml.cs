﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;

namespace CAD
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        LineWindow lw = new LineWindow();
        RectangleWindow rw = new RectangleWindow();
        CircleWindow cw = new CircleWindow();
        RectangleChangeWindow rcw = new RectangleChangeWindow();
        public List<Form> fl = new List<Form>();
        public List<Shape> sl = new List<Shape>();
        Rectangle r1;
        Rectangle r2;
        Rectangle r3;
        Rectangle r4;



        public MainWindow()
        {
            InitializeComponent();



        }

        private void LineB_Click(object sender, RoutedEventArgs e)
        {
            lw.Show();
        }

        private void RectangleB_Click(object sender, RoutedEventArgs e)
        {
            rw.Show();
        }

        private void CircleB_Click(object sender, RoutedEventArgs e)
        {
            cw.Show();
        }

        private void drawLine_Click(object sender, RoutedEventArgs e)
        {



            Linie l = new Linie();
            Line myLine = new Line();
            myLine.Stroke = Brushes.Black;
            myLine.X1 = lw.x1;
            myLine.X2 = lw.x2;
            myLine.Y1 = lw.y1;
            myLine.Y2 = lw.y2;
            l.x1 = lw.x1;
            l.x2 = lw.x2;
            l.y1 = lw.y1;
            l.y2 = lw.y2;
            myLine.HorizontalAlignment = HorizontalAlignment.Left;
            myLine.VerticalAlignment = VerticalAlignment.Center;
            myLine.StrokeThickness = 2;
            canvas1.Children.Add(myLine);

            fl.Add(l);
            sl.Add(myLine);

        }

        private void drawRectangle_Click(object sender, RoutedEventArgs e)
        {
            Rechteck r = new Rechteck();
            Rectangle myRectangle = new Rectangle();
            myRectangle.Stroke = Brushes.Black;
            myRectangle.Width = rw.w;
            myRectangle.Height = rw.h;
            Canvas.SetTop(myRectangle, rw.y);
            Canvas.SetLeft(myRectangle, rw.x);
            r.width = rw.w;
            r.height = rw.h;
            r.x = rw.x;
            r.y = rw.y;
            myRectangle.HorizontalAlignment = HorizontalAlignment.Left;
            myRectangle.VerticalAlignment = VerticalAlignment.Center;
            myRectangle.StrokeThickness = 2;

            canvas1.Children.Add(myRectangle);



            fl.Add(r);
            sl.Add(myRectangle);

        }

        private void drawCircle_Click(object sender, RoutedEventArgs e)
        {

            Kreis k = new Kreis();
            Ellipse myEllipse = new Ellipse();
            myEllipse.Stroke = Brushes.Black;
            myEllipse.Width = cw.r;
            myEllipse.Height = cw.r;
            Canvas.SetTop(myEllipse, cw.y);
            Canvas.SetLeft(myEllipse, cw.x);
            k.width = cw.r;
            k.height = cw.r;
            k.x = cw.x;
            k.y = cw.y;
            myEllipse.HorizontalAlignment = HorizontalAlignment.Left;
            myEllipse.VerticalAlignment = VerticalAlignment.Center;
            myEllipse.StrokeThickness = 2;
            canvas1.Children.Add(myEllipse);

            fl.Add(k);
            sl.Add(myEllipse);

        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            Point p = e.GetPosition(this);
            double x = p.X - 112.5;
            double y = p.Y;
            int h = 0;

            foreach (Form f in fl)
            {
                if (f.shape == "Rechteck")
                {
                    Rechteck r = (Rechteck)f;
                    if (r.AmISelected(x, y, r))
                    {
                        rw.Show();
                        rw.xR.Text = r.x.ToString();
                        rw.yR.Text = r.y.ToString();
                        rw.wR.Text = r.width.ToString();
                        rw.hR.Text = r.height.ToString();
                        //canvas1.Children.Remove(sl[h]);
                        sl.RemoveAt(h);
                        fl.RemoveAt(h);

                        DecoratorRectangle dr = new DecoratorRectangle();

                        r1 = dr.DrawRectangle();

                        Canvas.SetLeft(r1, r.x + r.width / 2 - 5);
                        Canvas.SetTop(r1, r.y - 3);

                        canvas1.Children.Add(r1);

                        r2 = dr.DrawRectangle();

                        Canvas.SetLeft(r2, r.x + r.width - 7);
                        Canvas.SetTop(r2, r.y + r.height / 2 - 5);

                        canvas1.Children.Add(r2);

                        r3 = dr.DrawRectangle();

                        Canvas.SetLeft(r3, r.x + r.width / 2 - 5);
                        Canvas.SetTop(r3, r.y + r.height - 7);

                        canvas1.Children.Add(r3);

                        r4 = dr.DrawRectangle();

                        Canvas.SetLeft(r4, r.x - 3);
                        Canvas.SetTop(r4, r.y + r.height / 2 - 5);

                        canvas1.Children.Add(r4);

                        label1text.Content = "";
                        label2text.Content = "";
                        label3text.Content = "";
                        label4text.Content = "";
                        label1.Content = "";
                        label2.Content = "";
                        label3.Content = "";
                        label4.Content = "";

                        label1.Content = "ObenLinks:";
                        label2.Content = "ObenRechts:";
                        label3.Content = "UntenLinks:";
                        label4.Content = "UntenRechts:";
                        label1text.Content = r.x + "," + r.y;
                        label2text.Content = r.x + r.width + "," + r.y;
                        label3text.Content = r.x + "," + r.y + r.height;
                        label4text.Content = r.x + r.width + "," + r.y + r.height;

                        

                        return;

                    }
                }
                if (f.shape == "Kreis")
                {
                    Kreis k = (Kreis)f;
                    if (k.AmISelected(x, y, k))
                    {
                        cw.Show();
                        cw.xC.Text = k.x.ToString();
                        cw.yC.Text = k.y.ToString();
                        cw.rC.Text = k.width.ToString();
                        //canvas1.Children.Remove(sl[h]);
                        sl.RemoveAt(h);
                        fl.RemoveAt(h);

                        DecoratorRectangle dr = new DecoratorRectangle();

                        r1 = dr.DrawRectangle();

                        Canvas.SetLeft(r1, k.x + k.width / 2 - 5);
                        Canvas.SetTop(r1, k.y - 3);

                        canvas1.Children.Add(r1);

                        r2 = dr.DrawRectangle();

                        Canvas.SetLeft(r2, k.x + k.width - 7);
                        Canvas.SetTop(r2, k.y + k.height / 2 - 5);

                        canvas1.Children.Add(r2);

                        r3 = dr.DrawRectangle();

                        Canvas.SetLeft(r3, k.x + k.width / 2 - 5);
                        Canvas.SetTop(r3, k.y + k.height - 7);

                        canvas1.Children.Add(r3);

                        r4 = dr.DrawRectangle();

                        Canvas.SetLeft(r4, k.x - 3);
                        Canvas.SetTop(r4, k.y + k.height / 2 - 5);

                        canvas1.Children.Add(r4);

                        label1text.Content = "";
                        label2text.Content = "";
                        label3text.Content = "";
                        label4text.Content = "";
                        label1.Content = "";
                        label2.Content = "";
                        label3.Content = "";
                        label4.Content = "";

                        label1.Content = "Center:";
                        label1text.Content = (k.x + k.width / 2) + "," + (k.y + k.height / 2);
                        

                        return;
                    }
                }

                if (f.shape == "Linie")
                {
                    Linie l = (Linie)f;
                    if (l.AmISelected(x, y, l))
                    {
                        lw.Show();
                        lw.tbLx1.Text = l.x1.ToString();
                        lw.tbLy1.Text = l.y1.ToString();
                        lw.tbLx2.Text = l.x2.ToString();
                        lw.tbLy2.Text = l.y2.ToString();
                        //canvas1.Children.Remove(sl[h]);
                        sl.RemoveAt(h);
                        fl.RemoveAt(h);
                        DecoratorRectangle dr = new DecoratorRectangle();

                        r1 = dr.DrawRectangle();

                        Canvas.SetLeft(r1, l.x1);
                        Canvas.SetTop(r1, l.y1);

                        canvas1.Children.Add(r1);

                        r2 = dr.DrawRectangle();

                        Canvas.SetLeft(r2, l.x2);
                        Canvas.SetTop(r2, l.y2);

                        canvas1.Children.Add(r2);

                        label1text.Content = "";
                        label2text.Content = "";
                        label3text.Content = "";
                        label4text.Content = "";
                        label1.Content = "";
                        label2.Content = "";
                        label3.Content = "";
                        label4.Content = "";

                        label1.Content = "Anfangspunkt:";
                        label1text.Content = l.x1 + "," + l.y1;
                        label2.Content = "Endpunkt:";
                        label2text.Content = l.x2 + "," + l.y2;



                        return;
                    }

                }
                h++;
            }
        }
    }
}
