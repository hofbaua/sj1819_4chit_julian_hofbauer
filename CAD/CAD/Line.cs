﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace CAD
{
    public class Linie : Form
    {
        public int x1 { get; set; }

        public int y1 { get; set; }

        public int x2 { get; set; }

        public int y2 { get; set; }

        SolidColorBrush strokeColor;
        int strokeThickness;

        public Linie()
        {
            strokeColor = StrokeColor;
            strokeThickness = StrokeThickness;
            shape = "Linie";
        }

        public bool AmISelected(double mx, double my, Linie l)
        {

            if (l.x1 < l.x2 && l.y1 < l.y2)
            {
                if (l.x1 <= mx && l.x2 >= mx && l.y1 <= my && l.y2 >= my)
                {
                    return true;
                }
                return false;
            }
            if (l.x2 < l.x1 && l.y2 < l.y1)
            {
                if (l.x2 <= mx && l.x1 >= mx && l.y2 <= my && l.y1 >= my)
                {
                    return true;
                }
                return false;
            }
            if (l.x2 < l.x1 && l.y1 < l.y2)
            {
                if (l.x2 <= mx && l.x1 >= mx && l.y1 <= my && l.y2 >= my)
                {
                    return true;
                }
                return false;
            }
            if (l.x1 < l.x2 && l.y2 < l.y1)
            {
                if (l.x1 <= mx && l.x2 >= mx && l.y2 <= my && l.y1 >= my)
                {
                    return true;
                }
                return false;
            }
            else
            {
                return false;
            }
            

        }
    }
}