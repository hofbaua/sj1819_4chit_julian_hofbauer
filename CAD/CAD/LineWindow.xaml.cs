﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CAD
{
    /// <summary>
    /// Interaktionslogik für LineWindow.xaml
    /// </summary>
    public partial class LineWindow : Window
    {

        public int x1;
        public int x2;
        public int y1;
        public int y2;

        public LineWindow()
        {
            InitializeComponent();
            
        }

        private void LineWindowOK_Click(object sender, RoutedEventArgs e)
        {


            



            x1 = Convert.ToInt32(tbLx1.Text);
            x2 = Convert.ToInt32(tbLx2.Text);
            y1 = Convert.ToInt32(tbLy1.Text);
            y2 = Convert.ToInt32(tbLy2.Text);


            this.Visibility = Visibility.Hidden;


        }
    }
}
