﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Shapes;
using System.Windows.Media;

namespace CAD
{
    public abstract class Form
    {
        public SolidColorBrush StrokeColor { get; set; }
        public string shape { get; set; }

        public int StrokeThickness = 3;


    }
}