﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace CAD
{
    public class Kreis : Form
    {
        public int x { get; set; }

        public int y { get; set; }

        public int height { get; set; }

        public int width { get; set; }

        SolidColorBrush strokeColor;
        int strokeThickness;

        public Kreis()
        {
            strokeColor = StrokeColor;
            strokeThickness = StrokeThickness;
            shape = "Kreis";
        }

        public bool AmISelected(double mx, double my, Kreis k)
        {
            if (k.x <= mx && k.x + k.width*2 >= mx && k.y <= my && k.y + k.height*2 >= my)
            {
                return true;
            }
            return false;
        }
    }
}