﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;

namespace CAD
{
    class DecoratorRectangle
    {

        public Rectangle DrawRectangle()
        {
            Rectangle myRectangle = new Rectangle();

            myRectangle.Height = 10;
            myRectangle.Width = 10;

            myRectangle.StrokeThickness = 2;
            myRectangle.Stroke = System.Windows.Media.Brushes.Black;

            return myRectangle;
        }
        

        
        
    }
}
