﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Shapes;
using System.Windows.Media;

namespace CAD
{
    public class Rechteck : Form
    {
        public int x { get; set; }

        public int y { get; set; }

        public int width { get; set; }

        public int height { get; set; }

        SolidColorBrush strokeColor;
        int strokeThickness;

        public Rechteck()
        {
            strokeColor = StrokeColor;
            strokeThickness = StrokeThickness;
            shape = "Rechteck";
        }

        public bool AmISelected(double mx, double my, Rechteck r)
        {
            if (r.x <= mx && r.x + r.width >= mx && r.y <= my && r.y + r.height >= my)
            {
                return true;
            }
            return false;
        }
        
    }
}