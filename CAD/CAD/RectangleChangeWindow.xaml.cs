﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CAD
{
    /// <summary>
    /// Interaktionslogik für RectangleChangeWindow.xaml
    /// </summary>
    public partial class RectangleChangeWindow : Window
    {
        public static MainWindow mw = new MainWindow();
        
        public int x;
        public int y;
        public int h;
        public int w;
        public RectangleChangeWindow()
        {
            InitializeComponent();

            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Rechteck r = new Rechteck();
            Rectangle myRectangle = new Rectangle();
            myRectangle.Stroke = Brushes.Black;
            myRectangle.Width = w;
            myRectangle.Height = h;
            Canvas.SetTop(myRectangle, y);
            Canvas.SetLeft(myRectangle, x);
            r.width = w;
            r.height = h;
            r.x = x;
            r.y = y;
            myRectangle.HorizontalAlignment = HorizontalAlignment.Left;
            myRectangle.VerticalAlignment = VerticalAlignment.Center;
            myRectangle.StrokeThickness = 2;

            mw.canvas1.Children.Add(myRectangle);



            mw.fl.Add(r);
            mw.sl.Add(myRectangle);

            this.Visibility = Visibility.Hidden;
        }
    }
}
