﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Text.RegularExpressions;

namespace Archiver
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void archiveButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            List<string> filePathList = new List<string>();
            List<string> fileList = new List<string>();
            object[] or = new object[3];

            progressBar1.Maximum = 100;
            progressBar1.Step = 1;
            progressBar1.Value = 0;


            if (fbd.ShowDialog() == DialogResult.OK)
            {


                DirectoryInfo dinfo = new DirectoryInfo(fbd.SelectedPath);
                FileInfo[] Files = dinfo.GetFiles("*.txt");

                foreach (FileInfo file in Files)
                {
                    filePathList.Add(fbd.SelectedPath + "\\" + file.Name);
                }

                DirectoryInfo dinfo2 = new DirectoryInfo(fbd.SelectedPath);
                FileInfo[] Files2 = dinfo.GetFiles("*.txt");

                foreach (FileInfo file in Files2)
                {
                    fileList.Add(file.Name);
                }




                FolderBrowserDialog fbd1 = new FolderBrowserDialog();

                if (fbd1.ShowDialog() == DialogResult.OK)
                {

                    or[0] = fbd1.SelectedPath;
                    or[1] = fileList;
                    or[2] = filePathList;

                    backgroundWorker1.RunWorkerAsync(or);

                }
            }




        }




       

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

            int h = 1;
            int h1 = 0;
            object[] or = (object[])e.Argument;


            string file = (string)or[0];
            List<string> slPathFile = (List<string>)or[1];
            List<string> slFile = (List<string>)or[2];
            


            StreamWriter sw = new StreamWriter(file + @"\archive.arc");
            StreamReader sr;


            foreach (string s in slFile)
            {


                sr = new StreamReader(s);
                sw.WriteLine(slPathFile[h1]+"!");
                string str = sr.ReadToEnd();
                sw.WriteLine(str);
                sr.Close();
                sw.WriteLine(";;");
                h++;
                backgroundWorker1.WorkerReportsProgress = true;
                backgroundWorker1.ReportProgress(h / slFile.Count * 100);
                h1++;
                Thread.Sleep(2000);
            }


            sw.Flush();
            sw.Close();
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

            progressBar1.Value = e.ProgressPercentage;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            progressBar1.Value = 0;
        }



        private void dearchiveButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            string file;
            string savePath;

            object[] or = new object[2];
            

            

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                file = ofd.FileName;
                or[0] = file;
            }

            FolderBrowserDialog fbd = new FolderBrowserDialog();

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                savePath = fbd.SelectedPath;
                or[1] = savePath;

                backgroundWorker2.RunWorkerAsync(or);

            }

            
        }

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {

            object[] or = (object[])e.Argument;

            string file = (string)or[0];
            string path = (string)or[1];

            StreamReader sr = new StreamReader(file);
            StreamWriter sw;

            string str = sr.ReadToEnd();
            sr.Close();
            string[] strar = str.Split(new string[] { ";;" }, StringSplitOptions.None);
            int h = 0;
            foreach (string s in strar)
            {
                if (s != "\r\n")
                {
                    string s2;
                    string s3;
                    int index = s.LastIndexOf("!");

                    s2 = s.Substring(0, index);
                    s3 = s.Substring(index);

                    string replacedString = Regex.Replace(s2, @"(\r\n|\r|\n)", "");
                    sw = new StreamWriter(path+"\\"+replacedString + ".txt");
                    sw.Write(s3);
                    sw.Flush();
                    sw.Close();

                    var lines = File.ReadAllLines(path + "\\" + replacedString + ".txt");
                    File.WriteAllLines(path + "\\" + replacedString + ".txt", lines.Skip(1).ToArray());
                    h++;
                    backgroundWorker2.WorkerReportsProgress = true;
                    backgroundWorker2.ReportProgress(h / strar.Length * 100);
                    
                }
                else
                {

                    MessageBox.Show("Datei dearchiviert");
                }
                
                
            }








        }

        private void backgroundWorker2_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

            progressBar1.Value = e.ProgressPercentage;
        }

        private void backgroundWorker2_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            progressBar1.Value = 0;

            
        }
    }
}
