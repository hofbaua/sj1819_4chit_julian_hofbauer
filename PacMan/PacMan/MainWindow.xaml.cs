﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfAnimatedGif;

namespace PacMan
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        double posx;
        double posy;
        Image i;
        

        public MainWindow()
        {



            InitializeComponent();

            BrushConverter bc = new BrushConverter();
            pacField.Background = (Brush)bc.ConvertFrom("#8181ff");

            i = ImagePlacer();

            KeyDown += Window_KeyDown;

            
            

        }



        public Image ImagePlacer()
        {
            Image finalImage = new Image();
            finalImage.Loaded += FinalImage_Loaded;
            


            pacField.Children.Add(finalImage);

            return finalImage;
        }

        private void FinalImage_Loaded(object sender, RoutedEventArgs e)
        {
            BitmapImage logo = new BitmapImage();
            logo.BeginInit();
            logo.UriSource = new Uri(@"C:\Users\Julian\repos\sj1819_4chit_julian_hofbauer\PacMan\PacMan\bin\Debug\pacman.gif");
            logo.EndInit();
            ImageBehavior.SetAnimatedSource(i, logo);

            var finalImage = sender as Image;
            finalImage.Source = logo;


            finalImage.Height = logo.Height;
            finalImage.Width = logo.Width;

            posx = pacField.ActualWidth;
            posy = pacField.ActualHeight;

            Canvas.SetTop(finalImage, posy/2-20);
            Canvas.SetLeft(finalImage, posx/2-20);



        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            TransformGroup tg = new TransformGroup();
            RotateTransform rt;
            


            switch (e.Key)
            {
                case Key.Down:
                    rt = new RotateTransform(90, i.Width/2, i.Height/2);
                    
                    tg.Children.Add(rt);
                    
                    i.RenderTransform = tg;
                    Canvas.SetLeft(i, Canvas.GetLeft(i));
                    Canvas.SetTop(i, Canvas.GetTop(i) + 50);
                    
                    break;
                case Key.Up:
                    rt = new RotateTransform(270, i.Width / 2, i.Height / 2);
                    
                    tg.Children.Add(rt);
                    
                    i.RenderTransform = tg;
                    Canvas.SetLeft(i, Canvas.GetLeft(i));
                    Canvas.SetTop(i, Canvas.GetTop(i) - 50);
                    break;
                case Key.Left:
                    rt = new RotateTransform(180, i.Width / 2, i.Height / 2);
                    
                    tg.Children.Add(rt);
                    
                    i.RenderTransform = tg;
                    Canvas.SetLeft(i, Canvas.GetLeft(i) - 50);
                    Canvas.SetTop(i, Canvas.GetTop(i));
                    break;
                case Key.Right:
                    rt = new RotateTransform(360, i.Width / 2, i.Height / 2);
                    
                    tg.Children.Add(rt);
                    
                    i.RenderTransform = tg;
                    Canvas.SetLeft(i, Canvas.GetLeft(i) + 50);
                    Canvas.SetTop(i, Canvas.GetTop(i));
                    break;
                case Key.Space:
                    Canvas.SetTop(i, posy / 2 - 20);
                    Canvas.SetLeft(i, posx / 2 - 20);
                    i.RenderTransform = null;
  
                    break;



                    
            }

            

        }
    }
}
