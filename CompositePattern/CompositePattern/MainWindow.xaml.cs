﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CompositePattern
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
            //Picture p = new Picture(mainCanvas, 1, 1, 410, 500, Brushes.Transparent);
            //Picture p1 = new Picture(mainCanvas, 1, 1, 350, 250, Brushes.Aqua);
            //p1.Add(new Picture(mainCanvas, 1, 1, 280, 125, Brushes.Green));
            //p.Add(p1);

            //p.Draw(1);



            Text t = new Text(textB, "Level 1");
            Text t1 = new Text(textB, "Level 2");
            Text t2 = new Text(textB, "Level 2_2");
            Text t3 = new Text(textB, "Level 3");


            t.Add(t1);
            t.Add(t2);
            t1.Add(t3);
            t.Draw(1);
        }

        
    }
}
