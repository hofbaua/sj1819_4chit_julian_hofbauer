﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CompositePattern
{
    abstract class Grafik
    {

        Control c;
        Color col;
        int left;
        int top;

        public abstract void Add(Grafik g);

        public abstract void Draw(int level);


    }
}
