﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Controls;


namespace CompositePattern
{
    class Text : Grafik
    {

       
        string s;
        TextBlock c;

        public List<Grafik> children = new List<Grafik>();


        public Text(TextBlock c, string s)
        {
            this.c = c;
            this.s = s;
        }

        public override void Add(Grafik g)
        {
            children.Add(g);
        }

        public override void Draw(int level)
        {

            c.Text += new String(' ', level) + ' ' + s + "\n";

            foreach (Grafik gf in children)
            {
                gf.Draw(level + 2);
            }
        }

    }
}
