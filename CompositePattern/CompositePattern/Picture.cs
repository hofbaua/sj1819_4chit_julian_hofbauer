﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.Windows.Media;


namespace CompositePattern
{
    class Picture : Grafik
    {
        Brush col;
        Canvas c;
        int height;
        int width;
        List<Grafik> children = new List<Grafik>();

        public Picture(Canvas c, int top, int left, int width, int height, Brush col)
        {
            this.c = c;
            this.col = col;
            this.height = height;
            this.width = width;
        }

        public override void Add(Grafik g)
        {
            children.Add(g);
        }

        public override void Draw(int level)
        {

            c.Children.Add(new System.Windows.Shapes.Rectangle() { Width = width, Height = height, Fill = col, Stroke = Brushes.Black });

            foreach (Grafik gf in children)
            {
                gf.Draw(level + 2);
            }
                
          
            
        }

    }
}
