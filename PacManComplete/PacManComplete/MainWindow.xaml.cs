﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfAnimatedGif;


namespace PacManComplete
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    /// 
    public enum Keys
    {
        Up, Down, Left, Right, Space
    }
    public partial class MainWindow : Window
    {

        double posx;
        double posy;
        Image i;
        PacMan pm;
        Game g;
        


        public MainWindow()
        {
            InitializeComponent();

            Loaded += delegate
            {
                BrushConverter bc = new BrushConverter();
                pacField.Background = (Brush)bc.ConvertFrom("#8181ff");
                posx = pacField.ActualWidth;
                posy = pacField.ActualHeight;

                pm = new PacMan(pacField, 50, 25);
                g = new Game(pacField, 50, posx, posy, pm);
                g.l = counterLabel;

                pm.g = g;
                
                g.i = pm.i;
                i = pm.i;
                Canvas.SetZIndex(i, 90);

                KeyDown += Window_KeyDown;

                

                
            };

        }



        private void Window_KeyDown(object sender, KeyEventArgs e)
        {

            Keys key;

            switch (e.Key)
            {
                case Key.Up:
                    key = Keys.Up;
                    pm.Turn(key);
                    break;
                case Key.Down:
                    key = Keys.Down;
                    pm.Turn(key);
                    break;
                case Key.Left:
                    key = Keys.Left;
                    pm.Turn(key);
                    break;
                case Key.Right:
                    key = Keys.Right;
                    pm.Turn(key);
                    break;
                case Key.Space:
                    key = Keys.Space;
                    pm.Turn(key);
                    g.dt.Start();
                    g.dt.Interval = TimeSpan.FromMilliseconds(350);
                    StartLabel.Content = "";
                    break;
            }




        }
    }
}
 
