﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfAnimatedGif;
using System.Windows.Threading;
using System.Windows.Media.Animation;

namespace PacManComplete
{
    
    class Game
    {
        public DispatcherTimer dt = new DispatcherTimer();
        public PacMan pm;
        public Image i;
        public Label l;
        int counter = 0;
        public List<Pill> pills = new List<Pill>();
        int pointx1 = 34;
        int pointx2 = 34;
        int pointx3 = 34;
        int pointx4 = 34;
        int pointx5 = 34;
        int pointx6 = 34;


        Canvas c;
      
        public double posx;
        public double posy;
        int width;
        Line[] larx;
        Line[] lary;


        

        public Game(Canvas c, int width, double posx, double posy, PacMan pm)
        {
            
            this.c = c;
            this.posx = posx;
            this.posy = posy;
            this.width = width;
            larx = new Line[(int)posx / width + 1];
            lary = new Line[(int)posy / width - 1];
            DrawLines();
            FillPills();
            DrawPills();
            dt.Tick += dt_Tick;
            this.pm = pm;
            pm.isOnJunction += lul;
            
            

        }

        private void lul(int x)
        {
            
                counter += x;
                l.Content = counter;
            
            
        }

        

        public void DrawLines()
        {
            
            for (int i = 0; i < larx.Length; i++)
            {
                larx[i] = new Line();
                larx[i].X1 = (i + 1) * width;
                larx[i].X2 = (i + 1) * width;
                larx[i].Y1 = 0;
                larx[i].Y2 = posy;
                larx[i].Stroke = System.Windows.Media.Brushes.Black;
                larx[i].HorizontalAlignment = HorizontalAlignment.Left;
                larx[i].VerticalAlignment = VerticalAlignment.Center;
                larx[i].StrokeThickness = 1;
                c.Children.Add(larx[i]);
            }

            for (int i = 0; i < lary.Length; i++)
            {
                lary[i] = new Line();
                lary[i].Y1 = (i + 1) * width;
                lary[i].Y2 = (i + 1) * width;
                lary[i].X1 = 0;
                lary[i].X2 = posx;
                lary[i].Stroke = System.Windows.Media.Brushes.Black;
                lary[i].HorizontalAlignment = HorizontalAlignment.Left;
                lary[i].VerticalAlignment = VerticalAlignment.Center;
                lary[i].StrokeThickness = 1;
                c.Children.Add(lary[i]);
                
            }
        }

        public void DrawPills()
        {
            
            Rectangle myRect;

            foreach (Pill lp in pills)
            {
                

                myRect = new System.Windows.Shapes.Rectangle();
                myRect.Stroke = (Brush)lp.color;
                myRect.Fill = (Brush)lp.color;
                myRect.HorizontalAlignment = HorizontalAlignment.Left;
                myRect.VerticalAlignment = VerticalAlignment.Center;
                myRect.Height = lp.height;
                myRect.Width = lp.width;
                Canvas.SetTop(myRect, lp.p.Y+10);
                Canvas.SetLeft(myRect, lp.p.X+10);
                c.Children.Add(myRect);
            }

            


        }

        private void FillPills()
        {
            int h = 0;
            Random rand = new Random();
            int x;
            for (int i = 0; i < 90; i++)
            {
                x = rand.Next(1, 10);
                if (x == 5)
                {
                    pills.Add(new BigPill());
                }
                else
                {
                    pills.Add(new Pill());
                }
                


            }

            foreach (Pill pl in pills)
            {
                if (h < 15)
                {
                    pl.p.X = pointx1;
                    pl.p.Y = 34;
                    pointx1 += 50;
                    h++;
                }
                else if (h < 30)
                {
                    pl.p.X = pointx2;
                    pl.p.Y = 84;
                    pointx2 += 50;
                    h++;
                }
                else if (h < 45)
                {
                    pl.p.X = pointx3;
                    pl.p.Y = 134;
                    pointx3 += 50;
                    h++;
                }
                else if (h < 60)
                {
                    pl.p.X = pointx4;
                    pl.p.Y = 184;
                    pointx4 += 50;
                    h++;
                }
                else if (h < 75)
                {
                    pl.p.X = pointx5;
                    pl.p.Y = 234;
                    pointx5 += 50;
                    h++;
                }
                else if (h < 90)
                {
                    pl.p.X = pointx6;
                    pl.p.Y = 284;
                    pointx6 += 50;
                    h++;
                }

            }
        }

        
            

        private void dt_Tick(object sender, EventArgs e)
        {

            if (pills.Count == 0)
            {
                dt.Stop();
                c.Children.Clear();
                Label gameOver = new Label();
                gameOver.Content = "GAME OVER";
                gameOver.FontSize = 80;
                Canvas.SetLeft(gameOver, 200);
                Canvas.SetTop(gameOver, 100);
                c.Children.Add(gameOver);
                SolidColorBrush scb = new SolidColorBrush();
                ColorAnimation animation = new ColorAnimation();
                animation.RepeatBehavior = RepeatBehavior.Forever;
                animation.AutoReverse = true;
                animation.From = Colors.Green;
                animation.To = Colors.Blue;
                animation.Duration = new Duration(TimeSpan.FromSeconds(1));
                scb.BeginAnimation(SolidColorBrush.ColorProperty, animation);
                gameOver.Foreground = scb;

            }
            pm.MoveOn(dt);

        }


    }
}
