﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfAnimatedGif;
using System.Windows.Threading;

namespace PacManComplete
{
    class PacMan
    {
        Canvas c;
        public Image i;
        int rotation = 4;
        int stepwidth;
        public int gridwidth;
        public Game g;

        public delegate void OnJunction(int x);
        public event OnJunction isOnJunction;

        public PacMan(Canvas c, int gridwidth, int stepwidth)
        {
            this.c = c;
            
            this.gridwidth = gridwidth;
            this.stepwidth = stepwidth;
            i = ImagePlacer();

            i.RenderTransform = null;

        }

        public Image ImagePlacer()
        {
            Image finalImage = new Image();
            finalImage.Loaded += FinalImage_Loaded;



            c.Children.Add(finalImage);

            return finalImage;
        }

        private void FinalImage_Loaded(object sender, RoutedEventArgs e)
        {
            BitmapImage logo = new BitmapImage();
            logo.BeginInit();
            logo.UriSource = new Uri(@"C:\Users\Julian\repos\sj1819_4chit_julian_hofbauer\PacMan\PacMan\bin\Debug\PacMan.gif");
            logo.EndInit();
            ImageBehavior.SetAnimatedSource(i, logo);

            var finalImage = sender as Image;
            finalImage.Source = logo;


            finalImage.Height = logo.Height;
            finalImage.Width = logo.Width;

            

            Canvas.SetTop(finalImage, 34);
            Canvas.SetLeft(finalImage, 0);



        }

        public void Turn(Keys k)
        {
            TransformGroup tg = new TransformGroup();
            RotateTransform rt;

            if (Canvas.GetTop(i)%2 == 0 && Canvas.GetLeft(i)%2 == 0)
            {

                

                switch (k)
                {
                    case Keys.Down:
                        rt = new RotateTransform(90, i.Width / 2, i.Height / 2);
                        rotation = 1;
                        tg.Children.Add(rt);

                        i.RenderTransform = tg;


                        break;
                    case Keys.Up:
                        rt = new RotateTransform(270, i.Width / 2, i.Height / 2);
                        rotation = 2;
                        tg.Children.Add(rt);

                        i.RenderTransform = tg;

                        break;
                    case Keys.Left:
                        rt = new RotateTransform(180, i.Width / 2, i.Height / 2);
                        rotation = 3;
                        tg.Children.Add(rt);

                        i.RenderTransform = tg;

                        break;
                    case Keys.Right:
                        rt = new RotateTransform(360, i.Width / 2, i.Height / 2);
                        rotation = 4;
                        tg.Children.Add(rt);

                        i.RenderTransform = tg;

                        break;
                    case Keys.Space:
                        if (i.RenderTransform != null)
                        {
                            switch (rotation)
                            {
                                case 1:
                                    Canvas.SetLeft(i, Canvas.GetLeft(i));
                                    Canvas.SetTop(i, Canvas.GetTop(i) + 50);
                                    break;
                                case 2:
                                    Canvas.SetLeft(i, Canvas.GetLeft(i));
                                    Canvas.SetTop(i, Canvas.GetTop(i) - 50);
                                    break;
                                case 3:
                                    Canvas.SetLeft(i, Canvas.GetLeft(i) - 50);
                                    Canvas.SetTop(i, Canvas.GetTop(i));
                                    break;
                                case 4:
                                    Canvas.SetLeft(i, Canvas.GetLeft(i) + 50);
                                    Canvas.SetTop(i, Canvas.GetTop(i));
                                    break;

                            }
                        }

                        break;
                }
            }

            
        }

        public void MoveOn(DispatcherTimer dt)
        {
            bool x = false;
            int h = 0;

            if (Canvas.GetTop(i) % 2 == 0 && Canvas.GetLeft(i) % 2 == 0)
            {

                foreach (Pill pl in g.pills)
                {
                    if (pl.p.X == Canvas.GetLeft(i) && pl.p.Y == Canvas.GetTop(i))
                    {
                        if (pl is BigPill)
                        {
                            isOnJunction(5);
                        }
                        else
                        {
                            isOnJunction(1);
                        }
                        
                        x = true;
                        break;
                    }
                    h++;
                }
                if (x)
                {
                    
                    foreach (var r in c.Children.OfType<Rectangle>())
                    {
                        if (Canvas.GetLeft(r)-10 == g.pills[h].p.X && Canvas.GetTop(r)-10 == g.pills[h].p.Y)
                        {
                            c.Children.Remove(r);
                            break;
                        }
                    }
                    g.pills.Remove(g.pills[h]);
                }
                

                

            }

                if (Canvas.GetTop(i)+1 >= c.ActualHeight - 34 || Canvas.GetTop(i)+1 <= 0 || Canvas.GetLeft(i)+1 <= 0 || Canvas.GetLeft(i)+1 >= c.ActualWidth - 34)
            {

                dt.Stop();
            }
            else
            {
                if (i.RenderTransform == null)
                {



                    Canvas.SetLeft(i, Canvas.GetLeft(i) + 34);
                    Canvas.SetTop(i, Canvas.GetTop(i));
                    RotateTransform rt = new RotateTransform(360, i.Width / 2, i.Height / 2);
                    i.RenderTransform = rt;



                }
                else
                {
                    switch (rotation)
                    {
                        case 1:
                            Canvas.SetLeft(i, Canvas.GetLeft(i));
                            Canvas.SetTop(i, Canvas.GetTop(i) + stepwidth);
                            break;
                        case 2:
                            Canvas.SetLeft(i, Canvas.GetLeft(i));
                            Canvas.SetTop(i, Canvas.GetTop(i) - stepwidth);
                            break;
                        case 3:
                            Canvas.SetLeft(i, Canvas.GetLeft(i) - stepwidth);
                            Canvas.SetTop(i, Canvas.GetTop(i));
                            break;
                        case 4:
                            Canvas.SetLeft(i, Canvas.GetLeft(i) + stepwidth);
                            Canvas.SetTop(i, Canvas.GetTop(i));
                            break;

                    }
                }
            }



            

            
            
        }
    }
}
