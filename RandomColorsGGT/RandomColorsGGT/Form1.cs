﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;

namespace RandomColorsGGT
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            

            Random rand = new Random();

            int x = rand.Next(1, 5);

            switch (x)
            {
                case 1:
                    bgColor.BackColor = Color.Green;
                    break;
                case 2:
                    bgColor.BackColor = Color.Blue;
                    break;
                case 3:
                    bgColor.BackColor = Color.Red;
                    break;
                case 4:
                    bgColor.BackColor = Color.Pink;
                    break;
                case 5:
                    bgColor.BackColor = Color.Yellow;
                    break;

            }

        }

        private void bgColor_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            int a = Convert.ToInt32(Num1.Text);
            int b = Convert.ToInt32(Num2.Text);

            Thread ggtThread = new Thread( ()=> GGT(a, b));

            ggtThread.Start();
            ggtThread.Join();
            
            
            


        }

        public void GGT(int _zahl1, int _zahl2)
        {
            



            int zahl1 = _zahl1;
            int zahl2 = _zahl2;
            //Diese Variable wird bei Wertzuweisungen zwischen den Zahlen benutzt
            int temp = 0;
            //Der Rückgabewert zweier gegebener Zahlen.
            int ggt = 0;//Solange der Modulo der zwei zahlen nicht 0 ist,
                        //werden Zuweisungen entsprechend demEuklidischen Algorithmus ausgeführt.
            while (zahl1 % zahl2 != 0)
            {
                temp = zahl1 % zahl2;
                zahl1 = zahl2;
                zahl2 = temp;
            }

            ggt = zahl2;

            StreamWriter sw = new StreamWriter("ggT.txt");
            sw.Write(ggt);
            sw.Flush();
            sw.Close();

            Thread.Sleep(1000);

            
           
        }
    }
}
