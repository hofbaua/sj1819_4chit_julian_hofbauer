﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using System.Data.OleDb;

namespace DatabaseToXML
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlDocument docAll = new XmlDocument();
            NodeXml(docAll);
            //AttributeXml();
            writeAll(docAll);
            checkXML(docAll);


        }


        static void NodeXml(XmlDocument docNodes)
        {

            string cs = "Provider = Microsoft.Jet.OLEDB.4.0; Data Source = Transfer.mdb";
            OleDbConnection con = new OleDbConnection(cs);
            con.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "select * from Patient;";

            OleDbDataReader reader = cmd.ExecuteReader();


            XmlNode docNode = docNodes.CreateXmlDeclaration("1.0", "UTF-8", null);
            docNodes.AppendChild(docNode);
            docNodes.LoadXml("<Patients></Patients>");

            while (reader.Read())
            {
                XmlNode patient = docNodes.CreateElement("Patient");
                XmlNode id = docNodes.CreateElement("ID");
                id.InnerText = reader[0].ToString();
                patient.AppendChild(id);
                XmlNode nn = docNodes.CreateElement("Nachname");
                nn.InnerText = reader[1].ToString();
                patient.AppendChild(nn);
                XmlNode vn = docNodes.CreateElement("Vorname");
                vn.InnerText = reader[2].ToString();
                patient.AppendChild(vn);
                XmlNode svnr = docNodes.CreateElement("SVNR");
                svnr.InnerText = reader[3].ToString();
                patient.AppendChild(svnr);

                XmlNode r = docNodes.SelectSingleNode("Patients");
                r.AppendChild(patient);
            }

            docNodes.Save("patientNode.xml");
            reader.Close();
            con.Close();

        }

        static void AttributeXml()
        {

            string cs = "Provider = Microsoft.Jet.OLEDB.4.0; Data Source = Transfer.mdb";
            OleDbConnection con = new OleDbConnection(cs);
            con.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "select * from Patient;";

            OleDbDataReader reader = cmd.ExecuteReader();

            XmlDocument docAtrributes = new XmlDocument();
            XmlNode docAttribute = docAtrributes.CreateXmlDeclaration("1.0", "UTF-8", null);
            docAtrributes.AppendChild(docAttribute);
            docAtrributes.LoadXml("<Patients></Patients>");

            while (reader.Read())
            {
                XmlNode patientAttributes = docAtrributes.CreateElement("Patient");
                XmlAttribute idAttributes = docAtrributes.CreateAttribute("ID");
                idAttributes.Value = reader[0].ToString();
                patientAttributes.Attributes.Append(idAttributes);
                XmlAttribute nnAttributes = docAtrributes.CreateAttribute("Nachname");
                nnAttributes.Value = reader[1].ToString();
                patientAttributes.Attributes.Append(nnAttributes);
                XmlAttribute vnAttributes = docAtrributes.CreateAttribute("Vorname");
                vnAttributes.Value = reader[2].ToString();
                patientAttributes.Attributes.Append(vnAttributes);
                XmlAttribute svnrAttributes = docAtrributes.CreateAttribute("SVNR");
                svnrAttributes.Value = reader[3].ToString();
                patientAttributes.Attributes.Append(svnrAttributes);

                XmlNode rAttributes = docAtrributes.SelectSingleNode("Patients");
                rAttributes.AppendChild(patientAttributes);
            }


            docAtrributes.Save("patientAtt.xml");

            reader.Close();
            con.Close();



        }

        static void writeAll(XmlDocument doc)
        {
            string cs = "Provider = Microsoft.Jet.OLEDB.4.0; Data Source = Transfer.mdb";
            OleDbConnection con = new OleDbConnection(cs);
            con.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;


            XmlNodeList xmlnl = doc.SelectNodes("/Patients/Patient");

            foreach (XmlNode x in xmlnl)
            {
                cmd.CommandText = "select patientid, behandlungsid, dauer, datum from patientbehandlungen where patientid = " + x.FirstChild.InnerText;
                OleDbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    OleDbCommand cmd1 = new OleDbCommand();
                    cmd1.Connection = con;
                    cmd1.CommandText = "Select id, bezeichnung from Behandlung where id =" + reader[1].ToString();
                    OleDbDataReader reader1;
                    reader1 = cmd1.ExecuteReader();

                    while (reader1.Read())
                    {
                        XmlNode behandlung = doc.CreateElement("behandlung");
                        behandlung.InnerText = reader1[1].ToString();
                        x.AppendChild(behandlung);
                    }


                    XmlNode dauer = doc.CreateElement("dauer");
                    dauer.InnerText = reader[2].ToString();
                    x.AppendChild(dauer);

                    XmlNode datum = doc.CreateElement("datum");
                    datum.InnerText = reader[3].ToString();
                    x.AppendChild(datum);



                }
                doc.Save("all.xml");
                reader.Close();
            }
        }

        static void checkXML(XmlDocument doc)
        {

            string cs = "Provider = Microsoft.Jet.OLEDB.4.0; Data Source = Transfer.mdb";
            OleDbConnection con = new OleDbConnection(cs);
            con.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;

            cmd.CommandText = "select ID from Patient";

            OleDbDataReader reader = cmd.ExecuteReader();

            List<int> Ids = new List<int>();

            while (reader.Read())
            {
                Ids.Add((int)reader[0]);
            }


            XmlNodeList xmlnl = doc.SelectNodes("/Patients/Patient/ID");

            int h = 0;

            foreach (XmlNode x in xmlnl)
            {
                if (Convert.ToInt32(x.InnerText) != Ids[h])
                {
                    Console.WriteLine("Daten stimmen nicht überein");
                    
                }
                else
                {
                    Console.WriteLine("Daten stimmen überein");
                }
                h++;
            }

            

        }

        











    }

}
