﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace XPathQueries
{
    class Program
    {
        static void Main(string[] args)
        {
            moreThan30Points();
            pupil941();
            sumCompare();
            avg();
            showTable();
        }

        static void moreThan30Points()
        {

            XmlDocument xmld = new XmlDocument();

            xmld.Load("Result.tea");

            XmlNodeList xmlnl = xmld.SelectNodes("/TestResult/Pupil[Punkte>30.00]");

            Console.WriteLine(xmlnl.Count);

            //xmld.Save("test.xml");

            Console.WriteLine();
        }

        static void pupil941()
        {
            XmlDocument xmld = new XmlDocument();

            xmld.Load("Result.tea");

            XmlNodeList xmlnl = xmld.SelectNodes("//Pupil[@ID=941]");

            foreach (XmlNode x in xmlnl)
            {
                Console.WriteLine(xmlnl[0].ChildNodes[2].InnerText+" "+ xmlnl[0].ChildNodes[1].InnerText);
            }

            
           
        }

        static void sumCompare()
        {
            string sumString = "nix geht";
            double sum = 0;
            double erg = 0;
            string real = "nix geht";

            XmlDocument xmld = new XmlDocument();

            xmld.Load("Result.tea");

            XmlNodeList xmlnl = xmld.SelectNodes("//Pupil");

            foreach (XmlNode x in xmlnl)
            {
                sumString = x.ChildNodes[3].InnerText.Replace('.', ',');
                
                Double.TryParse(sumString, out erg);
                sum += erg;
            }

            XmlNode xmln = xmld.SelectSingleNode("//TestResult[@SumPunkte]");

            real = xmln.Attributes["SumPunkte"].Value;

            Console.WriteLine("Berechnet: "+sum+ "Eigentlich: "+ real);
        }

        static void avg()
        {

            string sumString = "nix geht";
            double avg = 0;
            double erg = 0;

            XmlDocument xmld = new XmlDocument();

            xmld.Load("Result.tea");

            XmlNodeList xmlnl = xmld.SelectNodes("//Pupil");
            int count = 0;
            foreach (XmlNode x in xmlnl)
            {
                sumString = x.ChildNodes[3].InnerText.Replace('.', ',');

                if (Double.TryParse(sumString, out erg))
                    count++;
                avg += erg;
            }

            avg = avg / count;

            Console.WriteLine(avg);


        }

        static void showTable()
        {
            XmlDocument xmld = new XmlDocument();

            xmld.Load("Result.tea");

            Console.WriteLine("KatNr: ");
            Console.WriteLine("Nachname: ");
            Console.WriteLine("Vorname: ");
            Console.WriteLine("Punkte: ");
            Console.WriteLine("Prozent: ");
            Console.WriteLine("Beurteilung: ");
            Console.WriteLine("Note: ");

        }
    }
}
